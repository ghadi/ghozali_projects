
;CodeVisionAVR C Compiler V2.05.0 Professional
;(C) Copyright 1998-2010 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com

;Chip type                : ATmega32
;Program type             : Application
;Clock frequency          : 16.000000 MHz
;Memory model             : Small
;Optimize for             : Size
;(s)printf features       : int, width
;(s)scanf features        : int, width
;External RAM size        : 0
;Data Stack size          : 800 byte(s)
;Heap size                : 0 byte(s)
;Promote 'char' to 'int'  : No
;'char' is unsigned       : Yes
;8 bit enums              : Yes
;global 'const' stored in FLASH: No
;Enhanced core instructions    : On
;Smart register allocation     : On
;Automatic register allocation : On

	#pragma AVRPART ADMIN PART_NAME ATmega32
	#pragma AVRPART MEMORY PROG_FLASH 32768
	#pragma AVRPART MEMORY EEPROM 1024
	#pragma AVRPART MEMORY INT_SRAM SIZE 2143
	#pragma AVRPART MEMORY INT_SRAM START_ADDR 0x60

	#define CALL_SUPPORTED 1

	.LISTMAC
	.EQU UDRE=0x5
	.EQU RXC=0x7
	.EQU USR=0xB
	.EQU UDR=0xC
	.EQU SPSR=0xE
	.EQU SPDR=0xF
	.EQU EERE=0x0
	.EQU EEWE=0x1
	.EQU EEMWE=0x2
	.EQU EECR=0x1C
	.EQU EEDR=0x1D
	.EQU EEARL=0x1E
	.EQU EEARH=0x1F
	.EQU WDTCR=0x21
	.EQU MCUCR=0x35
	.EQU GICR=0x3B
	.EQU SPL=0x3D
	.EQU SPH=0x3E
	.EQU SREG=0x3F

	.DEF R0X0=R0
	.DEF R0X1=R1
	.DEF R0X2=R2
	.DEF R0X3=R3
	.DEF R0X4=R4
	.DEF R0X5=R5
	.DEF R0X6=R6
	.DEF R0X7=R7
	.DEF R0X8=R8
	.DEF R0X9=R9
	.DEF R0XA=R10
	.DEF R0XB=R11
	.DEF R0XC=R12
	.DEF R0XD=R13
	.DEF R0XE=R14
	.DEF R0XF=R15
	.DEF R0X10=R16
	.DEF R0X11=R17
	.DEF R0X12=R18
	.DEF R0X13=R19
	.DEF R0X14=R20
	.DEF R0X15=R21
	.DEF R0X16=R22
	.DEF R0X17=R23
	.DEF R0X18=R24
	.DEF R0X19=R25
	.DEF R0X1A=R26
	.DEF R0X1B=R27
	.DEF R0X1C=R28
	.DEF R0X1D=R29
	.DEF R0X1E=R30
	.DEF R0X1F=R31

	.EQU __SRAM_START=0x0060
	.EQU __SRAM_END=0x085F
	.EQU __DSTACK_SIZE=0x0320
	.EQU __HEAP_SIZE=0x0000
	.EQU __CLEAR_SRAM_SIZE=__SRAM_END-__SRAM_START+1

	.MACRO __CPD1N
	CPI  R30,LOW(@0)
	LDI  R26,HIGH(@0)
	CPC  R31,R26
	LDI  R26,BYTE3(@0)
	CPC  R22,R26
	LDI  R26,BYTE4(@0)
	CPC  R23,R26
	.ENDM

	.MACRO __CPD2N
	CPI  R26,LOW(@0)
	LDI  R30,HIGH(@0)
	CPC  R27,R30
	LDI  R30,BYTE3(@0)
	CPC  R24,R30
	LDI  R30,BYTE4(@0)
	CPC  R25,R30
	.ENDM

	.MACRO __CPWRR
	CP   R@0,R@2
	CPC  R@1,R@3
	.ENDM

	.MACRO __CPWRN
	CPI  R@0,LOW(@2)
	LDI  R30,HIGH(@2)
	CPC  R@1,R30
	.ENDM

	.MACRO __ADDB1MN
	SUBI R30,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDB2MN
	SUBI R26,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDW1MN
	SUBI R30,LOW(-@0-(@1))
	SBCI R31,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW2MN
	SUBI R26,LOW(-@0-(@1))
	SBCI R27,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	SBCI R22,BYTE3(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1N
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	SBCI R22,BYTE3(-@0)
	SBCI R23,BYTE4(-@0)
	.ENDM

	.MACRO __ADDD2N
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	SBCI R24,BYTE3(-@0)
	SBCI R25,BYTE4(-@0)
	.ENDM

	.MACRO __SUBD1N
	SUBI R30,LOW(@0)
	SBCI R31,HIGH(@0)
	SBCI R22,BYTE3(@0)
	SBCI R23,BYTE4(@0)
	.ENDM

	.MACRO __SUBD2N
	SUBI R26,LOW(@0)
	SBCI R27,HIGH(@0)
	SBCI R24,BYTE3(@0)
	SBCI R25,BYTE4(@0)
	.ENDM

	.MACRO __ANDBMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ANDWMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ANDI R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ANDD1N
	ANDI R30,LOW(@0)
	ANDI R31,HIGH(@0)
	ANDI R22,BYTE3(@0)
	ANDI R23,BYTE4(@0)
	.ENDM

	.MACRO __ANDD2N
	ANDI R26,LOW(@0)
	ANDI R27,HIGH(@0)
	ANDI R24,BYTE3(@0)
	ANDI R25,BYTE4(@0)
	.ENDM

	.MACRO __ORBMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ORWMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ORI  R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ORD1N
	ORI  R30,LOW(@0)
	ORI  R31,HIGH(@0)
	ORI  R22,BYTE3(@0)
	ORI  R23,BYTE4(@0)
	.ENDM

	.MACRO __ORD2N
	ORI  R26,LOW(@0)
	ORI  R27,HIGH(@0)
	ORI  R24,BYTE3(@0)
	ORI  R25,BYTE4(@0)
	.ENDM

	.MACRO __DELAY_USB
	LDI  R24,LOW(@0)
__DELAY_USB_LOOP:
	DEC  R24
	BRNE __DELAY_USB_LOOP
	.ENDM

	.MACRO __DELAY_USW
	LDI  R24,LOW(@0)
	LDI  R25,HIGH(@0)
__DELAY_USW_LOOP:
	SBIW R24,1
	BRNE __DELAY_USW_LOOP
	.ENDM

	.MACRO __GETD1S
	LDD  R30,Y+@0
	LDD  R31,Y+@0+1
	LDD  R22,Y+@0+2
	LDD  R23,Y+@0+3
	.ENDM

	.MACRO __GETD2S
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	LDD  R24,Y+@0+2
	LDD  R25,Y+@0+3
	.ENDM

	.MACRO __PUTD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R31
	STD  Y+@0+2,R22
	STD  Y+@0+3,R23
	.ENDM

	.MACRO __PUTD2S
	STD  Y+@0,R26
	STD  Y+@0+1,R27
	STD  Y+@0+2,R24
	STD  Y+@0+3,R25
	.ENDM

	.MACRO __PUTDZ2
	STD  Z+@0,R26
	STD  Z+@0+1,R27
	STD  Z+@0+2,R24
	STD  Z+@0+3,R25
	.ENDM

	.MACRO __CLRD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R30
	STD  Y+@0+2,R30
	STD  Y+@0+3,R30
	.ENDM

	.MACRO __POINTB1MN
	LDI  R30,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW1MN
	LDI  R30,LOW(@0+(@1))
	LDI  R31,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTD1M
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __POINTW1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	.ENDM

	.MACRO __POINTD1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	LDI  R22,BYTE3(2*@0+(@1))
	LDI  R23,BYTE4(2*@0+(@1))
	.ENDM

	.MACRO __POINTB2MN
	LDI  R26,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW2MN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTBRM
	LDI  R@0,LOW(@1)
	.ENDM

	.MACRO __POINTWRM
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __POINTBRMN
	LDI  R@0,LOW(@1+(@2))
	.ENDM

	.MACRO __POINTWRMN
	LDI  R@0,LOW(@2+(@3))
	LDI  R@1,HIGH(@2+(@3))
	.ENDM

	.MACRO __POINTWRFN
	LDI  R@0,LOW(@2*2+(@3))
	LDI  R@1,HIGH(@2*2+(@3))
	.ENDM

	.MACRO __GETD1N
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __GETD2N
	LDI  R26,LOW(@0)
	LDI  R27,HIGH(@0)
	LDI  R24,BYTE3(@0)
	LDI  R25,BYTE4(@0)
	.ENDM

	.MACRO __GETB1MN
	LDS  R30,@0+(@1)
	.ENDM

	.MACRO __GETB1HMN
	LDS  R31,@0+(@1)
	.ENDM

	.MACRO __GETW1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	.ENDM

	.MACRO __GETD1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	LDS  R22,@0+(@1)+2
	LDS  R23,@0+(@1)+3
	.ENDM

	.MACRO __GETBRMN
	LDS  R@0,@1+(@2)
	.ENDM

	.MACRO __GETWRMN
	LDS  R@0,@2+(@3)
	LDS  R@1,@2+(@3)+1
	.ENDM

	.MACRO __GETWRZ
	LDD  R@0,Z+@2
	LDD  R@1,Z+@2+1
	.ENDM

	.MACRO __GETD2Z
	LDD  R26,Z+@0
	LDD  R27,Z+@0+1
	LDD  R24,Z+@0+2
	LDD  R25,Z+@0+3
	.ENDM

	.MACRO __GETB2MN
	LDS  R26,@0+(@1)
	.ENDM

	.MACRO __GETW2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	.ENDM

	.MACRO __GETD2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	LDS  R24,@0+(@1)+2
	LDS  R25,@0+(@1)+3
	.ENDM

	.MACRO __PUTB1MN
	STS  @0+(@1),R30
	.ENDM

	.MACRO __PUTW1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	.ENDM

	.MACRO __PUTD1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	STS  @0+(@1)+2,R22
	STS  @0+(@1)+3,R23
	.ENDM

	.MACRO __PUTB1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRB
	.ENDM

	.MACRO __PUTW1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRW
	.ENDM

	.MACRO __PUTD1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRD
	.ENDM

	.MACRO __PUTBR0MN
	STS  @0+(@1),R0
	.ENDM

	.MACRO __PUTBMRN
	STS  @0+(@1),R@2
	.ENDM

	.MACRO __PUTWMRN
	STS  @0+(@1),R@2
	STS  @0+(@1)+1,R@3
	.ENDM

	.MACRO __PUTBZR
	STD  Z+@1,R@0
	.ENDM

	.MACRO __PUTWZR
	STD  Z+@2,R@0
	STD  Z+@2+1,R@1
	.ENDM

	.MACRO __GETW1R
	MOV  R30,R@0
	MOV  R31,R@1
	.ENDM

	.MACRO __GETW2R
	MOV  R26,R@0
	MOV  R27,R@1
	.ENDM

	.MACRO __GETWRN
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __PUTW1R
	MOV  R@0,R30
	MOV  R@1,R31
	.ENDM

	.MACRO __PUTW2R
	MOV  R@0,R26
	MOV  R@1,R27
	.ENDM

	.MACRO __ADDWRN
	SUBI R@0,LOW(-@2)
	SBCI R@1,HIGH(-@2)
	.ENDM

	.MACRO __ADDWRR
	ADD  R@0,R@2
	ADC  R@1,R@3
	.ENDM

	.MACRO __SUBWRN
	SUBI R@0,LOW(@2)
	SBCI R@1,HIGH(@2)
	.ENDM

	.MACRO __SUBWRR
	SUB  R@0,R@2
	SBC  R@1,R@3
	.ENDM

	.MACRO __ANDWRN
	ANDI R@0,LOW(@2)
	ANDI R@1,HIGH(@2)
	.ENDM

	.MACRO __ANDWRR
	AND  R@0,R@2
	AND  R@1,R@3
	.ENDM

	.MACRO __ORWRN
	ORI  R@0,LOW(@2)
	ORI  R@1,HIGH(@2)
	.ENDM

	.MACRO __ORWRR
	OR   R@0,R@2
	OR   R@1,R@3
	.ENDM

	.MACRO __EORWRR
	EOR  R@0,R@2
	EOR  R@1,R@3
	.ENDM

	.MACRO __GETWRS
	LDD  R@0,Y+@2
	LDD  R@1,Y+@2+1
	.ENDM

	.MACRO __PUTBSR
	STD  Y+@1,R@0
	.ENDM

	.MACRO __PUTWSR
	STD  Y+@2,R@0
	STD  Y+@2+1,R@1
	.ENDM

	.MACRO __MOVEWRR
	MOV  R@0,R@2
	MOV  R@1,R@3
	.ENDM

	.MACRO __INWR
	IN   R@0,@2
	IN   R@1,@2+1
	.ENDM

	.MACRO __OUTWR
	OUT  @2+1,R@1
	OUT  @2,R@0
	.ENDM

	.MACRO __CALL1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	ICALL
	.ENDM

	.MACRO __CALL1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	CALL __GETW1PF
	ICALL
	.ENDM

	.MACRO __CALL2EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMRDW
	ICALL
	.ENDM

	.MACRO __GETW1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X
	.ENDM

	.MACRO __GETD1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X+
	LD   R22,X
	.ENDM

	.MACRO __NBST
	BST  R@0,@1
	IN   R30,SREG
	LDI  R31,0x40
	EOR  R30,R31
	OUT  SREG,R30
	.ENDM


	.MACRO __PUTB1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RNS
	MOVW R26,R@0
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	CALL __PUTDP1
	.ENDM


	.MACRO __GETB1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R30,Z
	.ENDM

	.MACRO __GETB1HSX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	.ENDM

	.MACRO __GETW1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R31,Z
	MOV  R30,R0
	.ENDM

	.MACRO __GETD1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R1,Z+
	LD   R22,Z+
	LD   R23,Z
	MOVW R30,R0
	.ENDM

	.MACRO __GETB2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R26,X
	.ENDM

	.MACRO __GETW2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	.ENDM

	.MACRO __GETD2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R1,X+
	LD   R24,X+
	LD   R25,X
	MOVW R26,R0
	.ENDM

	.MACRO __GETBRSX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	LD   R@0,Z
	.ENDM

	.MACRO __GETWRSX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	LD   R@0,Z+
	LD   R@1,Z
	.ENDM

	.MACRO __GETBRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	LD   R@0,X
	.ENDM

	.MACRO __GETWRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	LD   R@0,X+
	LD   R@1,X
	.ENDM

	.MACRO __LSLW8SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	CLR  R30
	.ENDM

	.MACRO __PUTB1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __CLRW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __CLRD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R30
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __PUTB2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z,R26
	.ENDM

	.MACRO __PUTW2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z,R27
	.ENDM

	.MACRO __PUTD2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z+,R27
	ST   Z+,R24
	ST   Z,R25
	.ENDM

	.MACRO __PUTBSRX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	ST   Z,R@0
	.ENDM

	.MACRO __PUTWSRX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	ST   Z+,R@0
	ST   Z,R@1
	.ENDM

	.MACRO __PUTB1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __MULBRR
	MULS R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRRU
	MUL  R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRR0
	MULS R@0,R@1
	.ENDM

	.MACRO __MULBRRU0
	MUL  R@0,R@1
	.ENDM

	.MACRO __MULBNWRU
	LDI  R26,@2
	MUL  R26,R@0
	MOVW R30,R0
	MUL  R26,R@1
	ADD  R31,R0
	.ENDM

;NAME DEFINITIONS FOR GLOBAL VARIABLES ALLOCATED TO REGISTERS
	.DEF _rx_wr_index0=R5
	.DEF _rx_rd_index0=R4
	.DEF _rx_counter0=R7
	.DEF _counter_timer0=R6
	.DEF _counter_send_command=R9
	.DEF _Ir_Iteration=R8
	.DEF _ir_rx_wr_index=R11
	.DEF _ir_rx_rd_index=R10
	.DEF _ir_rx_counter=R13

	.CSEG
	.ORG 0x00

;START OF CODE MARKER
__START_OF_CODE:

;INTERRUPT VECTORS
	JMP  __RESET
	JMP  _ext_int0_isr
	JMP  _ext_int1_isr
	JMP  0x00
	JMP  _timer2_comp_isr
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  _timer0_ovf_isr
	JMP  0x00
	JMP  _usart_rx_isr
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00

_tbl10_G100:
	.DB  0x10,0x27,0xE8,0x3,0x64,0x0,0xA,0x0
	.DB  0x1,0x0
_tbl16_G100:
	.DB  0x0,0x10,0x0,0x1,0x10,0x0,0x1,0x0

;REGISTER BIT VARIABLES INITIALIZATION
__REG_BIT_VARS:
	.DW  0x0000

_0x42:
	.DB  0x30,0x33,0x35,0x3E,0x37,0x37,0x38,0x3C
	.DB  0x3E,0x3B,0x3E,0x38,0x34,0x3F,0x3F,0x38
	.DB  0x39,0x33,0x30,0x34,0x39,0x38,0x32,0x39
	.DB  0x36,0x3D,0x34,0x36,0x33,0x30,0x3D,0x3F
	.DB  0x31,0x3B,0x23,0x53,0x2A,0x23,0x47,0x7C
	.DB  0x30,0x2A,0x23,0x47,0x7C

__GLOBAL_INI_TBL:
	.DW  0x01
	.DW  0x02
	.DW  __REG_BIT_VARS*2

_0xFFFFFFFF:
	.DW  0

__RESET:
	CLI
	CLR  R30
	OUT  EECR,R30

;INTERRUPT VECTORS ARE PLACED
;AT THE START OF FLASH
	LDI  R31,1
	OUT  GICR,R31
	OUT  GICR,R30
	OUT  MCUCR,R30

;DISABLE WATCHDOG
	LDI  R31,0x18
	OUT  WDTCR,R31
	OUT  WDTCR,R30

;CLEAR R2-R14
	LDI  R24,(14-2)+1
	LDI  R26,2
	CLR  R27
__CLEAR_REG:
	ST   X+,R30
	DEC  R24
	BRNE __CLEAR_REG

;CLEAR SRAM
	LDI  R24,LOW(__CLEAR_SRAM_SIZE)
	LDI  R25,HIGH(__CLEAR_SRAM_SIZE)
	LDI  R26,__SRAM_START
__CLEAR_SRAM:
	ST   X+,R30
	SBIW R24,1
	BRNE __CLEAR_SRAM

;GLOBAL VARIABLES INITIALIZATION
	LDI  R30,LOW(__GLOBAL_INI_TBL*2)
	LDI  R31,HIGH(__GLOBAL_INI_TBL*2)
__GLOBAL_INI_NEXT:
	LPM  R24,Z+
	LPM  R25,Z+
	SBIW R24,0
	BREQ __GLOBAL_INI_END
	LPM  R26,Z+
	LPM  R27,Z+
	LPM  R0,Z+
	LPM  R1,Z+
	MOVW R22,R30
	MOVW R30,R0
__GLOBAL_INI_LOOP:
	LPM  R0,Z+
	ST   X+,R0
	SBIW R24,1
	BRNE __GLOBAL_INI_LOOP
	MOVW R30,R22
	RJMP __GLOBAL_INI_NEXT
__GLOBAL_INI_END:

;HARDWARE STACK POINTER INITIALIZATION
	LDI  R30,LOW(__SRAM_END-__HEAP_SIZE)
	OUT  SPL,R30
	LDI  R30,HIGH(__SRAM_END-__HEAP_SIZE)
	OUT  SPH,R30

;DATA STACK POINTER INITIALIZATION
	LDI  R28,LOW(__SRAM_START+__DSTACK_SIZE)
	LDI  R29,HIGH(__SRAM_START+__DSTACK_SIZE)

	JMP  _main

	.ESEG
	.ORG 0

	.DSEG
	.ORG 0x380

	.CSEG
;/*****************************************************
;This program was produced by the
;CodeWizardAVR V1.25.5 Professional
;Automatic Program Generator
;� Copyright 1998-2010 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com
;
;Project : GATE
;Version :
;Date    : 25/04/2014
;Author  : Ghozali S.H.
;Company :
;Comments:
;
;
;Chip type               : ATmega32
;Program type            : Application
;AVR Core Clock frequency: 16.000000 MHz
;Memory model            : Small
;External RAM size       : 0
;Data Stack size         : 512
;*****************************************************/
;
;#include <mega32a.h>
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;#include <delay.h>
;//#include <ir_trx.c>
;// Standard Input/Output functions
;#include <stdio.h>
;
;#ifndef RXB8
;#define RXB8 1
;#endif
;
;#ifndef TXB8
;#define TXB8 0
;#endif
;
;#ifndef UPE
;#define UPE 2
;#endif
;
;#ifndef DOR
;#define DOR 3
;#endif
;
;#ifndef FE
;#define FE 4
;#endif
;
;#ifndef UDRE
;#define UDRE 5
;#endif
;
;#ifndef RXC
;#define RXC 7
;#endif
;
;#define FRAMING_ERROR (1<<FE)
;#define PARITY_ERROR (1<<UPE)
;#define DATA_OVERRUN (1<<DOR)
;#define DATA_REGISTER_EMPTY (1<<UDRE)
;#define RX_COMPLETE (1<<RXC)
;
;#define DEMUX_A   PORTB.0
;#define DEMUX_B   PORTB.1
;#define DEMUX_OE  PORTB.2
;#define MUX_A     PORTB.3
;#define MUX_B     PORTB.4
;
;#define IR1_SD_O PORTA.0    //SHUTDOWN TFDU
;#define IR1_RX_I PIND.2     //INT0
;#define IR1_RX2_I PIND.3    //INT1
;#define IR1_TX_O PORTA.2    //TX TFDU
;
;#define IR1_TX_ON IR1_TX_O = 1
;#define IR1_TX_OFF IR1_TX_O = 0
;
;#define IR3_TX_O PORTA.1    //TX IR
;
;#define IR3_TX_ON IR3_TX_O = 1
;#define IR3_TX_OFF IR3_TX_O = 0
;
;#define SD_ON 1
;#define SD_OFF 0
;
;#define HI 1
;#define LO 0
;
;#define IRTIME_DELAY1 1
;
;// USART Receiver buffer
;#define RX_BUFFER_SIZE0 70
;char rx_buffer0[RX_BUFFER_SIZE0];
;
;#if RX_BUFFER_SIZE0<=256
;unsigned char rx_wr_index0,rx_rd_index0,rx_counter0;
;#else
;unsigned int rx_wr_index0,rx_rd_index0,rx_counter0;
;#endif
;
;bit dataOBU_complete;
;bit wait_dataOBU_overflow;
;unsigned char counter_timer0;
;unsigned char counter_send_command;
;
;
;void mulai_interrupt_timer(){
; 0000 006B void mulai_interrupt_timer(){

	.CSEG
_mulai_interrupt_timer:
; 0000 006C TCCR0=0x03;  //250.000 kHz
	LDI  R30,LOW(3)
	CALL SUBOPT_0x0
; 0000 006D TCNT0=0x00;
; 0000 006E OCR0=0x00;
; 0000 006F TIMSK|=0x01;
	ORI  R30,1
	OUT  0x39,R30
; 0000 0070 counter_timer0=0;
	CLR  R6
; 0000 0071 }
	RET
;
;void berhenti_interrupt_timer(){
; 0000 0073 void berhenti_interrupt_timer(){
_berhenti_interrupt_timer:
; 0000 0074 TCCR0=0x00;
	LDI  R30,LOW(0)
	CALL SUBOPT_0x0
; 0000 0075 TCNT0=0x00;
; 0000 0076 OCR0=0x00;
; 0000 0077 TIMSK&=~(0x01);
	ANDI R30,0xFE
	OUT  0x39,R30
; 0000 0078 }
	RET
;
;void mulai_interrupt_usart(){
; 0000 007A void mulai_interrupt_usart(){
_mulai_interrupt_usart:
; 0000 007B UCSRA=0x00;
	LDI  R30,LOW(0)
	OUT  0xB,R30
; 0000 007C UCSRB=0x98;
	LDI  R30,LOW(152)
	RJMP _0x2060002
; 0000 007D UCSRC=0x06;
; 0000 007E UBRRH=0x00;
; 0000 007F UBRRL=0x33;     //19200
; 0000 0080 }
;
;void berhenti_interrupt_usart(){
; 0000 0082 void berhenti_interrupt_usart(){
_berhenti_interrupt_usart:
; 0000 0083 UCSRA=0x00;
	LDI  R30,LOW(0)
	OUT  0xB,R30
; 0000 0084 UCSRB=0x18;
	LDI  R30,LOW(24)
_0x2060002:
	OUT  0xA,R30
; 0000 0085 UCSRC=0x06;
	CALL SUBOPT_0x1
; 0000 0086 UBRRH=0x00;
; 0000 0087 UBRRL=0x33;     //19200
; 0000 0088 }
	RET
;
;
;/************************* IR ********************************/
;#define BIT_PER_CELL 2
;
;#define PPM 4
;#define PULSE_WIDTH 1
;#define START_PULSE_WIDTH 3
;#define CELL_INTERVAL 20
;#define START_PULSE_NUM 2
;#define IR_TIMER TCNT2
;
;#define PULSE_INTERVAL (CELL_INTERVAL/PPM)
;
;#define PULSE_WIDTH_DELAY delay_us(PULSE_WIDTH)
;#define START_PULSE_WIDTH_DELAY delay_us(START_PULSE_WIDTH)
;#define START_TIME 2
;#define DATA_START_TIME 5
;#define CELL_DELAY 20
;#define PULSE_OFFSET 2
;
;#define BYTE_WIDTH (PULSE_INTERVAL + CELL_INTERVAL*(PPM+1))
;
;#define IR_OUT IR3_TX_ON
;#define IR_OFF IR3_TX_OFF
;
;#define NOT_PULSED GIFR==0
;#define CLEAR_IR_FLAG GIFR=0xC0
;
;//#define START_IR_TIMER TCCR2B=0x03;;IR_TIMER=0x00
;#define START_IR_TIMER TCCR2=0x03;;IR_TIMER=0x00
;#define STOP_IR_TIMER TCCR2=0x00
;#define ENABLE_IR_TIMER_COMPARE_INT TIMSK|=0x80
;#define SET_IR_TIMER_COMPARE OCR2=BYTE_WIDTH+CELL_DELAY
;//#define SET_IR_TIMER_COMPARE OCR2B=BYTE_WIDTH+CELL_DELAY
;
;#define TRUE 1
;#define FALSE 0
;
;#define MAX_CELL 4
;
;#define NOT_SENSING_DATA 0
;#define SENSING_DATA 1
;#define IR_DATA_VALID
;
;unsigned char Ir_Iteration, IrInTime[4];
;
;// This flag is set when IR Receiver is sensing data
;bit ir_status;
;
;// IR Receiver buffer
;#define IR_RX_BUFFER_SIZE 256
;char ir_rx_buffer[IR_RX_BUFFER_SIZE];
;
;#if IR_RX_BUFFER_SIZE<=256
;unsigned char ir_rx_wr_index,ir_rx_rd_index,ir_rx_counter;
;#else
;unsigned int ir_rx_wr_index,ir_rx_rd_index,ir_rx_counter;
;#endif
;
;// This flag is set on IR Receiver buffer overflow
;bit ir_rx_buffer_overflow;
;
;// Handler for External Interrupt service routine
;
;void ir_ext_int_handler(void)
; 0000 00CB {
_ir_ext_int_handler:
; 0000 00CC // Place your code here
; 0000 00CD     CLEAR_IR_FLAG;
	LDI  R30,LOW(192)
	OUT  0x3A,R30
; 0000 00CE 
; 0000 00CF    if(ir_status == NOT_SENSING_DATA)
	SBRC R2,2
	RJMP _0x3
; 0000 00D0     {
; 0000 00D1 	ENABLE_IR_TIMER_COMPARE_INT;
	IN   R30,0x39
	ORI  R30,0x80
	OUT  0x39,R30
; 0000 00D2 	SET_IR_TIMER_COMPARE;
	LDI  R30,LOW(125)
	OUT  0x23,R30
; 0000 00D3 	START_IR_TIMER;
	LDI  R30,LOW(3)
	OUT  0x25,R30
	LDI  R30,LOW(0)
	OUT  0x24,R30
; 0000 00D4 	Ir_Iteration = 0;
	CLR  R8
; 0000 00D5 	ir_status = SENSING_DATA;
	SET
	BLD  R2,2
; 0000 00D6     }
; 0000 00D7     else if(ir_status == SENSING_DATA)
	RJMP _0x4
_0x3:
	SBRS R2,2
	RJMP _0x5
; 0000 00D8     {
; 0000 00D9         if (Ir_Iteration<PPM ) IrInTime[Ir_Iteration%PPM] = IR_TIMER;
	LDI  R30,LOW(4)
	CP   R8,R30
	BRSH _0x6
	MOV  R30,R8
	ANDI R30,LOW(0x3)
	LDI  R31,0
	SUBI R30,LOW(-_IrInTime)
	SBCI R31,HIGH(-_IrInTime)
	MOVW R26,R30
	IN   R30,0x24
	ST   X,R30
; 0000 00DA         Ir_Iteration++;
_0x6:
	INC  R8
; 0000 00DB     }
; 0000 00DC }
_0x5:
_0x4:
	RET
;// Handler for Timer output compare interrupt service routine
;void ir_timer_comp_handler(void)
; 0000 00DF {
_ir_timer_comp_handler:
; 0000 00E0 // Place your code here
; 0000 00E1 unsigned char u8Data,Iteration,temp;
; 0000 00E2 //printf("%d %d %d %d \n", IrInTime[0],IrInTime[1],IrInTime[2],IrInTime[3]);
; 0000 00E3 
; 0000 00E4    STOP_IR_TIMER;
	CALL __SAVELOCR4
;	u8Data -> R17
;	Iteration -> R16
;	temp -> R19
	LDI  R30,LOW(0)
	OUT  0x25,R30
; 0000 00E5    ir_status = NOT_SENSING_DATA;
	CLT
	BLD  R2,2
; 0000 00E6    u8Data = 0;
	LDI  R17,LOW(0)
; 0000 00E7     //data conversion
; 0000 00E8 
; 0000 00E9    if (Ir_Iteration==PPM)
	LDI  R30,LOW(4)
	CP   R30,R8
	BRNE _0x7
; 0000 00EA    {
; 0000 00EB     for (Iteration=0;Iteration<MAX_CELL;Iteration++)
	LDI  R16,LOW(0)
_0x9:
	CPI  R16,4
	BRSH _0xA
; 0000 00EC     {
; 0000 00ED        if (Iteration) u8Data <<=BIT_PER_CELL;
	CPI  R16,0
	BREQ _0xB
	LSL  R17
	LSL  R17
; 0000 00EE 
; 0000 00EF        temp = IrInTime[Iteration] - (Iteration*CELL_DELAY + DATA_START_TIME - PULSE_OFFSET);
_0xB:
	MOV  R30,R16
	LDI  R31,0
	SUBI R30,LOW(-_IrInTime)
	SBCI R31,HIGH(-_IrInTime)
	LD   R18,Z
	LDI  R26,LOW(20)
	MUL  R16,R26
	MOVW R30,R0
	SUBI R30,-LOW(5)
	SUBI R30,LOW(2)
	MOV  R26,R18
	SUB  R26,R30
	MOV  R19,R26
; 0000 00F0        u8Data |= (temp/PULSE_INTERVAL);
	MOV  R26,R19
	LDI  R30,LOW(5)
	CALL __DIVB21U
	OR   R17,R30
; 0000 00F1     }
	SUBI R16,-1
	RJMP _0x9
_0xA:
; 0000 00F2 
; 0000 00F3     ir_rx_buffer[ir_rx_wr_index]=u8Data;
	MOV  R30,R11
	LDI  R31,0
	SUBI R30,LOW(-_ir_rx_buffer)
	SBCI R31,HIGH(-_ir_rx_buffer)
	ST   Z,R17
; 0000 00F4     if (++ir_rx_wr_index == IR_RX_BUFFER_SIZE) ir_rx_wr_index=0;
	INC  R11
	MOV  R26,R11
	CALL SUBOPT_0x2
	BRNE _0xC
	CLR  R11
; 0000 00F5     if (++ir_rx_counter == IR_RX_BUFFER_SIZE)
_0xC:
	INC  R13
	MOV  R26,R13
	CALL SUBOPT_0x2
	BRNE _0xD
; 0000 00F6     {
; 0000 00F7       ir_rx_counter=0;
	CLR  R13
; 0000 00F8       ir_rx_buffer_overflow=1;
	SET
	BLD  R2,3
; 0000 00F9     };
_0xD:
; 0000 00FA    };
_0x7:
; 0000 00FB }
	CALL __LOADLOCR4
	RJMP _0x2060001
;
;
;/*****************************************************
;// Get byte from IR
;*****************************************************/
;
;unsigned char ir_getchar(void)
; 0000 0103 {
_ir_getchar:
; 0000 0104 unsigned char u8Data;
; 0000 0105 while (ir_rx_counter==0);
	ST   -Y,R17
;	u8Data -> R17
_0xE:
	TST  R13
	BREQ _0xE
; 0000 0106 u8Data=ir_rx_buffer[ir_rx_rd_index];
	MOV  R30,R10
	LDI  R31,0
	SUBI R30,LOW(-_ir_rx_buffer)
	SBCI R31,HIGH(-_ir_rx_buffer)
	LD   R17,Z
; 0000 0107 if (++ir_rx_rd_index == IR_RX_BUFFER_SIZE) ir_rx_rd_index=0;
	INC  R10
	MOV  R26,R10
	CALL SUBOPT_0x2
	BRNE _0x11
	CLR  R10
; 0000 0108 #asm("cli")
_0x11:
	cli
; 0000 0109 --ir_rx_counter;
	DEC  R13
; 0000 010A #asm("sei")
	sei
; 0000 010B return u8Data;
	MOV  R30,R17
	LD   R17,Y+
	RET
; 0000 010C }
;
;/*****************************************************
;// Send byte via IR
;*****************************************************/
;void ir_putchar(unsigned char u8Data)
; 0000 0112 {
_ir_putchar:
; 0000 0113     unsigned char Iteration, IrOutTime[MAX_CELL];
; 0000 0114 
; 0000 0115 	#asm("cli")
	SBIW R28,4
	ST   -Y,R17
;	u8Data -> Y+5
;	Iteration -> R17
;	IrOutTime -> Y+1
	cli
; 0000 0116 
; 0000 0117     //data conversion
; 0000 0118     for (Iteration=0;Iteration<MAX_CELL;Iteration++)
	LDI  R17,LOW(0)
_0x13:
	CPI  R17,4
	BRSH _0x14
; 0000 0119     {
; 0000 011A        switch (u8Data & 0xC0)
	LDD  R30,Y+5
	ANDI R30,LOW(0xC0)
; 0000 011B        {
; 0000 011C          case 0x00:// if DataCell = 0b 00bb bbbb --> ir_output(1000)
	CPI  R30,0
	BRNE _0x18
; 0000 011D          IrOutTime[Iteration] = 0;
	CALL SUBOPT_0x3
	LDI  R30,LOW(0)
	RJMP _0x135
; 0000 011E          break;
; 0000 011F 
; 0000 0120          case 0x40:// if DataCell = 0b 01bb bbbb --> ir_output(0100)
_0x18:
	CPI  R30,LOW(0x40)
	BRNE _0x19
; 0000 0121          IrOutTime[Iteration] = 5;
	CALL SUBOPT_0x3
	LDI  R30,LOW(5)
	RJMP _0x135
; 0000 0122          break;
; 0000 0123 
; 0000 0124          case 0x80:// if DataCell = 0b 10bb bbbb --> ir_output(0010)
_0x19:
	CPI  R30,LOW(0x80)
	BRNE _0x1A
; 0000 0125          IrOutTime[Iteration] = 10;
	CALL SUBOPT_0x3
	LDI  R30,LOW(10)
	RJMP _0x135
; 0000 0126          break;
; 0000 0127 
; 0000 0128          case 0xC0:// if DataCell = 0b 11bb bbbb --> ir_output(0001)
_0x1A:
	CPI  R30,LOW(0xC0)
	BRNE _0x17
; 0000 0129          IrOutTime[Iteration] = 15;
	CALL SUBOPT_0x3
	LDI  R30,LOW(15)
_0x135:
	ST   X,R30
; 0000 012A          break;
; 0000 012B        } //end switch
_0x17:
; 0000 012C        IrOutTime[Iteration] += Iteration*CELL_DELAY + DATA_START_TIME;
	MOV  R30,R17
	LDI  R31,0
	MOVW R26,R28
	ADIW R26,1
	ADD  R30,R26
	ADC  R31,R27
	__PUTW1R 23,24
	LD   R22,Z
	LDI  R26,LOW(20)
	MUL  R17,R26
	MOVW R30,R0
	SUBI R30,-LOW(5)
	ADD  R30,R22
	__GETW2R 23,24
	ST   X,R30
; 0000 012D        //printf("IrOutTime: %d %d %d %d", IrOutTime[0],IrOutTime[1],IrOutTime[2],IrOutTime[3]);
; 0000 012E        u8Data <<=BIT_PER_CELL;
	LDD  R30,Y+5
	LSL  R30
	LSL  R30
	STD  Y+5,R30
; 0000 012F     }; // end for
	SUBI R17,-1
	RJMP _0x13
_0x14:
; 0000 0130 
; 0000 0131     //start sending IR signal
; 0000 0132 
; 0000 0133     //start bit
; 0000 0134     START_IR_TIMER;
	LDI  R30,LOW(3)
	OUT  0x25,R30
	LDI  R30,LOW(0)
	OUT  0x24,R30
; 0000 0135     IR_OUT;
	SBI  0x1B,1
; 0000 0136     START_PULSE_WIDTH_DELAY;
	__DELAY_USB 16
; 0000 0137     IR_OFF;
	CBI  0x1B,1
; 0000 0138 	while(IR_TIMER < START_TIME);
_0x20:
	IN   R30,0x24
	CPI  R30,LOW(0x2)
	BRLO _0x20
; 0000 0139     IR_OUT;
	SBI  0x1B,1
; 0000 013A     START_PULSE_WIDTH_DELAY;
	__DELAY_USB 16
; 0000 013B     IR_OFF;
	CBI  0x1B,1
; 0000 013C 
; 0000 013D     //data bit
; 0000 013E     for (Iteration=0;Iteration<MAX_CELL;Iteration++)
	LDI  R17,LOW(0)
_0x28:
	CPI  R17,4
	BRSH _0x29
; 0000 013F     {
; 0000 0140         while(IR_TIMER<IrOutTime[Iteration]);
_0x2A:
	IN   R0,36
	CALL SUBOPT_0x3
	LD   R30,X
	CP   R0,R30
	BRLO _0x2A
; 0000 0141         IR_OUT;
	SBI  0x1B,1
; 0000 0142         PULSE_WIDTH_DELAY;
	__DELAY_USB 5
; 0000 0143         IR_OFF;
	CBI  0x1B,1
; 0000 0144     }; // end for
	SUBI R17,-1
	RJMP _0x28
_0x29:
; 0000 0145 	#asm("sei")
	sei
; 0000 0146     while(IR_TIMER<=BYTE_WIDTH);
_0x31:
	IN   R30,0x24
	CPI  R30,LOW(0x6A)
	BRLO _0x31
; 0000 0147 };
	LDD  R17,Y+0
	ADIW R28,6
	RET
;
;////////////////////////////////////////////////////////
;
;
;void pc_switch();
;void ir_switch();
;void sc_switch();
;unsigned char hexa_to_char(unsigned char a);
;unsigned char convert_to_hexa(unsigned char a,unsigned char b);
;
;
;// This flag is set on USART Receiver buffer overflow
;bit rx_buffer_overflow0;
;
;/*****************************************************
;// Declare your global variables here
;*****************************************************/
;
;int i_CMD_IN;
;unsigned char CMD_IN[400];
;
;//int count_timer;
;
;bit int_flag;
;
;// USART Receiver interrupt service routine
;interrupt [USART_RXC] void usart_rx_isr(void)
; 0000 0163 {
_usart_rx_isr:
	CALL SUBOPT_0x4
; 0000 0164 char status,data;
; 0000 0165 status=UCSRA;
	ST   -Y,R17
	ST   -Y,R16
;	status -> R17
;	data -> R16
	IN   R17,11
; 0000 0166 data=UDR;
	IN   R16,12
; 0000 0167 if ((status & (FRAMING_ERROR | PARITY_ERROR | DATA_OVERRUN))==0)
	MOV  R30,R17
	ANDI R30,LOW(0x1C)
	BRNE _0x34
; 0000 0168    {
; 0000 0169     //rx_buffer[rx_wr_index++]=data;
; 0000 016A     CMD_IN[rx_counter0]=data;
	MOV  R30,R7
	LDI  R31,0
	SUBI R30,LOW(-_CMD_IN)
	SBCI R31,HIGH(-_CMD_IN)
	ST   Z,R16
; 0000 016B     if(data=='#' || rx_counter0>0)
	CPI  R16,35
	BREQ _0x36
	TST  R7
	BREQ _0x35
_0x36:
; 0000 016C     {
; 0000 016D         if(data=='#') {
	CPI  R16,35
	BRNE _0x38
; 0000 016E            rx_counter0=0;
	CLR  R7
; 0000 016F            i_CMD_IN=0;
	CALL SUBOPT_0x5
; 0000 0170            rx_buffer_overflow0=0;
; 0000 0171         }
; 0000 0172         if (++rx_wr_index0 == RX_BUFFER_SIZE0) rx_wr_index0=0;
_0x38:
	INC  R5
	LDI  R30,LOW(70)
	CP   R30,R5
	BRNE _0x39
	CLR  R5
; 0000 0173         if (++rx_counter0 >= 70 || data=='*')
_0x39:
	INC  R7
	LDI  R30,LOW(70)
	CP   R7,R30
	BRSH _0x3B
	CPI  R16,42
	BRNE _0x3A
_0x3B:
; 0000 0174         {
; 0000 0175            //rx_counter=0;
; 0000 0176            //#asm("cli")
; 0000 0177            berhenti_interrupt_usart();
	RCALL _berhenti_interrupt_usart
; 0000 0178            rx_buffer_overflow0=1;
	SET
	BLD  R2,4
; 0000 0179            int_flag=1;
	BLD  R2,5
; 0000 017A            /////////////////////////////////////
; 0000 017B            //if ((CMD_IN[rx_counter0-4]=='#')&&(CMD_IN[rx_counter0-3]=='O')&&(CMD_IN[rx_counter0-2]=='K')&&(CMD_IN[rx_counter0-1]=='*')){   //data complete jika ada konfirmasi #ok*
; 0000 017C            //    dataOBU_complete=1;
; 0000 017D            //}
; 0000 017E            // atau ada balasan kartu kalau transaksi berhasil (meskipun sebelumnya command konfirmasi #OKE* tidak )
; 0000 017F            //if((CMD_IN[0]=='#')&&(CMD_IN[1]=='V')&&(CMD_IN[rx_counter0-3]=='3')&&(CMD_IN[rx_counter0-2]=='3')&&(CMD_IN[rx_counter0-1]=='*')){
; 0000 0180            //    dataOBU_complete=1;
; 0000 0181            //}
; 0000 0182            /////////////////////////////////////
; 0000 0183 
; 0000 0184         }
; 0000 0185     }
_0x3A:
; 0000 0186    }
_0x35:
; 0000 0187 }
_0x34:
	LD   R16,Y+
	LD   R17,Y+
	RJMP _0x13C
;
;#ifndef _DEBUG_TERMINAL_IO_
;// Get a character from the USART Receiver buffer
;#define _ALTERNATE_GETCHAR_
;#pragma used+
;char getchar(void)
; 0000 018E {
; 0000 018F char data;
; 0000 0190 while (rx_counter0==0);
;	data -> R17
; 0000 0191 data=rx_buffer0[rx_rd_index0];
; 0000 0192 if (++rx_rd_index0 == RX_BUFFER_SIZE0) rx_rd_index0=0;
; 0000 0193 #asm("cli")
; 0000 0194 --rx_counter0;
; 0000 0195 #asm("sei")
; 0000 0196 return data;
; 0000 0197 }
;#pragma used-
;#endif
;
;/*****************************************************
;// External Interrupt 0 service routine
;*****************************************************/
;interrupt [EXT_INT0] void ext_int0_isr(void)
; 0000 019F {
_ext_int0_isr:
	CALL SUBOPT_0x4
; 0000 01A0     ir_ext_int_handler();
	RCALL _ir_ext_int_handler
; 0000 01A1 }
	RJMP _0x13C
;
;/*****************************************************
;// External Interrupt 1 service routine
;*****************************************************/
;interrupt [EXT_INT1] void ext_int1_isr(void)
; 0000 01A7 {
_ext_int1_isr:
	CALL SUBOPT_0x4
; 0000 01A8     ir_ext_int_handler();
	RCALL _ir_ext_int_handler
; 0000 01A9 }
	RJMP _0x13C
;
;// Timer 0 overflow interrupt service routine
;interrupt [TIM0_OVF] void timer0_ovf_isr(void)    //interrupt setiap 1.02 ms
; 0000 01AD {
_timer0_ovf_isr:
	ST   -Y,R30
	IN   R30,SREG
	ST   -Y,R30
; 0000 01AE     // Place your code here
; 0000 01AF     counter_timer0++;
	INC  R6
; 0000 01B0     if(counter_timer0 >= 40) wait_dataOBU_overflow = 1;
	LDI  R30,LOW(40)
	CP   R6,R30
	BRLO _0x41
	SET
	BLD  R2,1
; 0000 01B1 }
_0x41:
	LD   R30,Y+
	OUT  SREG,R30
	LD   R30,Y+
	RETI
;
;
;/*****************************************************
;// Timer 2 output compare interrupt service routine
;*****************************************************/
;interrupt [TIM2_COMP] void timer2_comp_isr(void)
; 0000 01B8 {
_timer2_comp_isr:
	CALL SUBOPT_0x4
; 0000 01B9     ir_timer_comp_handler();
	RCALL _ir_timer_comp_handler
; 0000 01BA }
_0x13C:
	LD   R30,Y+
	OUT  SREG,R30
	LD   R31,Y+
	LD   R30,Y+
	LD   R27,Y+
	LD   R26,Y+
	LD   R25,Y+
	LD   R24,Y+
	LD   R23,Y+
	LD   R22,Y+
	LD   R15,Y+
	LD   R1,Y+
	LD   R0,Y+
	RETI
;
;// Declare your global variables here
;
;void main(void)
; 0000 01BF {
_main:
; 0000 01C0 
; 0000 01C1 // Declare your local variables here
; 0000 01C2 bit overflow;
; 0000 01C3 unsigned char Rcvd;
; 0000 01C4 int j;
; 0000 01C5 unsigned char gheader[3] = {'#','G','|'};
; 0000 01C6 unsigned char start[5] = {'#','G','|','0','*'};
; 0000 01C7 unsigned char trigger[3] = {'#','S','*'};
; 0000 01C8 unsigned char debt1[]={'0','3','5','>','7','7','8','<','>',';','>','8','4','?','?','8','9','3','0','4','9','8','2','9','6','=',
; 0000 01C9                             '4','6','3','0','=','?','1',';'};
; 0000 01CA unsigned char dataSent[100];
; 0000 01CB unsigned char counter_dataSent;
; 0000 01CC unsigned char sCeksum[2];
; 0000 01CD unsigned char state;
; 0000 01CE unsigned char Temp;
; 0000 01CF unsigned char hTemp;
; 0000 01D0 unsigned char ceksum;
; 0000 01D1 int key_prev;
; 0000 01D2 unsigned char Temp_Convert;
; 0000 01D3 unsigned char Temp_Convert2;
; 0000 01D4 
; 0000 01D5 
; 0000 01D6 // Input/Output Ports initialization
; 0000 01D7 // Port A initialization
; 0000 01D8 // Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
; 0000 01D9 // State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T
; 0000 01DA PORTA=0x00;
	SBIW R28,63
	SBIW R28,63
	SBIW R28,27
	LDI  R24,45
	LDI  R26,LOW(108)
	LDI  R27,HIGH(108)
	LDI  R30,LOW(_0x42*2)
	LDI  R31,HIGH(_0x42*2)
	CALL __INITLOCB
;	overflow -> R15.0
;	Rcvd -> R17
;	j -> R18,R19
;	gheader -> Y+150
;	start -> Y+145
;	trigger -> Y+142
;	debt1 -> Y+108
;	dataSent -> Y+8
;	counter_dataSent -> R16
;	sCeksum -> Y+6
;	state -> R21
;	Temp -> R20
;	hTemp -> Y+5
;	ceksum -> Y+4
;	key_prev -> Y+2
;	Temp_Convert -> Y+1
;	Temp_Convert2 -> Y+0
	LDI  R30,LOW(0)
	OUT  0x1B,R30
; 0000 01DB DDRA=0x07;
	LDI  R30,LOW(7)
	OUT  0x1A,R30
; 0000 01DC 
; 0000 01DD // Port B initialization
; 0000 01DE // Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
; 0000 01DF // State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T
; 0000 01E0 PORTB=0x00;
	LDI  R30,LOW(0)
	OUT  0x18,R30
; 0000 01E1 DDRB=0x1F;
	LDI  R30,LOW(31)
	OUT  0x17,R30
; 0000 01E2 
; 0000 01E3 // Port C initialization
; 0000 01E4 // Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
; 0000 01E5 // State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T
; 0000 01E6 PORTC=0x00;
	LDI  R30,LOW(0)
	OUT  0x15,R30
; 0000 01E7 DDRC=0x00;
	OUT  0x14,R30
; 0000 01E8 
; 0000 01E9 // Port D initialization
; 0000 01EA // Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
; 0000 01EB // State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T
; 0000 01EC PORTD=0x00;
	OUT  0x12,R30
; 0000 01ED DDRD=0x00;
	OUT  0x11,R30
; 0000 01EE 
; 0000 01EF // Timer/Counter 0 initialization
; 0000 01F0 // Clock source: System Clock
; 0000 01F1 // Clock value: Timer 0 Stopped
; 0000 01F2 // Mode: Normal top=0xFF
; 0000 01F3 // OC0 output: Disconnected
; 0000 01F4 TCCR0=0x00;
	OUT  0x33,R30
; 0000 01F5 TCNT0=0x00;
	OUT  0x32,R30
; 0000 01F6 OCR0=0x00;
	OUT  0x3C,R30
; 0000 01F7 
; 0000 01F8 // Timer/Counter 1 initialization
; 0000 01F9 // Clock source: System Clock
; 0000 01FA // Clock value: Timer1 Stopped
; 0000 01FB // Mode: Normal top=0xFFFF
; 0000 01FC // OC1A output: Discon.
; 0000 01FD // OC1B output: Discon.
; 0000 01FE // Noise Canceler: Off
; 0000 01FF // Input Capture on Falling Edge
; 0000 0200 // Timer1 Overflow Interrupt: Off
; 0000 0201 // Input Capture Interrupt: Off
; 0000 0202 // Compare A Match Interrupt: Off
; 0000 0203 // Compare B Match Interrupt: Off
; 0000 0204 TCCR1A=0x00;
	OUT  0x2F,R30
; 0000 0205 TCCR1B=0x00;
	OUT  0x2E,R30
; 0000 0206 TCNT1H=0x00;
	OUT  0x2D,R30
; 0000 0207 TCNT1L=0x00;
	OUT  0x2C,R30
; 0000 0208 ICR1H=0x00;
	OUT  0x27,R30
; 0000 0209 ICR1L=0x00;
	OUT  0x26,R30
; 0000 020A OCR1AH=0x00;
	OUT  0x2B,R30
; 0000 020B OCR1AL=0x00;
	OUT  0x2A,R30
; 0000 020C OCR1BH=0x00;
	OUT  0x29,R30
; 0000 020D OCR1BL=0x00;
	OUT  0x28,R30
; 0000 020E 
; 0000 020F // Timer/Counter 2 initialization
; 0000 0210 // Clock source: System Clock
; 0000 0211 // Clock value: Timer2 Stopped
; 0000 0212 // Mode: Normal top=0xFF
; 0000 0213 // OC2 output: Disconnected
; 0000 0214 ASSR=0x00;
	OUT  0x22,R30
; 0000 0215 TCCR2=0x00;
	OUT  0x25,R30
; 0000 0216 TCNT2=0x00;
	OUT  0x24,R30
; 0000 0217 OCR2=0x00;
	OUT  0x23,R30
; 0000 0218 
; 0000 0219 // External Interrupt(s) initialization
; 0000 021A // INT0: On
; 0000 021B // INT0 Mode: Falling Edge
; 0000 021C // INT1: On
; 0000 021D // INT1 Mode: Falling Edge
; 0000 021E // INT2: Off
; 0000 021F 
; 0000 0220 GICR|=0xC0;
	IN   R30,0x3B
	ORI  R30,LOW(0xC0)
	OUT  0x3B,R30
; 0000 0221 MCUCR=0x0A;
	LDI  R30,LOW(10)
	OUT  0x35,R30
; 0000 0222 MCUCSR=0x00;
	LDI  R30,LOW(0)
	OUT  0x34,R30
; 0000 0223 GIFR=0xC0;
	LDI  R30,LOW(192)
	OUT  0x3A,R30
; 0000 0224 //GICR|=0x80;
; 0000 0225 //MCUCR=0x08;
; 0000 0226 //MCUCSR=0x00;
; 0000 0227 //GIFR=0x80;
; 0000 0228 
; 0000 0229 // Timer(s)/Counter(s) Interrupt(s) initialization
; 0000 022A TIMSK=0x00;
	LDI  R30,LOW(0)
	OUT  0x39,R30
; 0000 022B 
; 0000 022C // USART initialization
; 0000 022D // Communication Parameters: 8 Data, 1 Stop, No Parity
; 0000 022E // USART Receiver: On
; 0000 022F // USART Transmitter: On
; 0000 0230 // USART Mode: Asynchronous
; 0000 0231 // USART Baud Rate: 76800
; 0000 0232 UCSRA=0x00;
	OUT  0xB,R30
; 0000 0233 UCSRB=0x98;
	LDI  R30,LOW(152)
	OUT  0xA,R30
; 0000 0234 UCSRC=0x06;
	CALL SUBOPT_0x1
; 0000 0235 UBRRH=0x00;
; 0000 0236 //UBRRL=0x67;
; 0000 0237 UBRRL=0x33;  //19200
; 0000 0238 //UBRRL=0x19;  //38400
; 0000 0239 
; 0000 023A // Analog Comparator initialization
; 0000 023B // Analog Comparator: Off
; 0000 023C // Analog Comparator Input Capture by Timer/Counter 1: Off
; 0000 023D ACSR=0x80;
	LDI  R30,LOW(128)
	OUT  0x8,R30
; 0000 023E SFIOR=0x00;
	LDI  R30,LOW(0)
	OUT  0x30,R30
; 0000 023F 
; 0000 0240 // ADC initialization
; 0000 0241 // ADC disabled
; 0000 0242 ADCSRA=0x00;
	OUT  0x6,R30
; 0000 0243 
; 0000 0244 // SPI initialization
; 0000 0245 // SPI disabled
; 0000 0246 SPCR=0x00;
	OUT  0xD,R30
; 0000 0247 
; 0000 0248 // TWI initialization
; 0000 0249 // TWI disabled
; 0000 024A TWCR=0x00;
	OUT  0x36,R30
; 0000 024B 
; 0000 024C //IR1_TX_ON; //setting TFDU IR1
; 0000 024D delay_us(1);
	__DELAY_USB 5
; 0000 024E IR1_SD_O = SD_ON;
	SBI  0x1B,0
; 0000 024F delay_us(10);
	__DELAY_USB 53
; 0000 0250 IR1_SD_O = SD_OFF;
	CBI  0x1B,0
; 0000 0251 delay_us(1);
	__DELAY_USB 5
; 0000 0252 IR1_TX_OFF;
	CBI  0x1B,2
; 0000 0253 delay_ms(2);
	LDI  R30,LOW(2)
	LDI  R31,HIGH(2)
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
; 0000 0254 
; 0000 0255 
; 0000 0256 // Global enable interrupts
; 0000 0257 ir_rx_counter=0;
	CLR  R13
; 0000 0258 
; 0000 0259 //pc_switch();
; 0000 025A //delay_ms(100);
; 0000 025B 
; 0000 025C // Global enable interrupts
; 0000 025D 
; 0000 025E 
; 0000 025F state=2;
	LDI  R21,LOW(2)
; 0000 0260 i_CMD_IN=0;
	CALL SUBOPT_0x5
; 0000 0261 rx_buffer_overflow0 = 0;
; 0000 0262 dataOBU_complete = 0;
	CALL SUBOPT_0x6
; 0000 0263 wait_dataOBU_overflow = 0;
; 0000 0264 counter_send_command = 0;
; 0000 0265 overflow=0;
	CLT
	BLD  R15,0
; 0000 0266 #asm("sei")
	sei
; 0000 0267 
; 0000 0268 while(1){
_0x49:
; 0000 0269 
; 0000 026A   if (ir_rx_counter)
	TST  R13
	BREQ _0x4C
; 0000 026B   {
; 0000 026C     state=2;
	LDI  R21,LOW(2)
; 0000 026D     Rcvd=ir_getchar();
	RCALL _ir_getchar
	MOV  R17,R30
; 0000 026E     if (Rcvd=='#'){
	CPI  R17,35
	BRNE _0x4D
; 0000 026F        i_CMD_IN=0;
	CALL SUBOPT_0x7
; 0000 0270     }
; 0000 0271     CMD_IN[i_CMD_IN]= Rcvd;
_0x4D:
	CALL SUBOPT_0x8
	ST   Z,R17
; 0000 0272     if ((CMD_IN[i_CMD_IN]=='*')||(i_CMD_IN>=390)){
	CALL SUBOPT_0x8
	LD   R26,Z
	CPI  R26,LOW(0x2A)
	BREQ _0x4F
	LDS  R26,_i_CMD_IN
	LDS  R27,_i_CMD_IN+1
	CPI  R26,LOW(0x186)
	LDI  R30,HIGH(0x186)
	CPC  R27,R30
	BRLT _0x4E
_0x4F:
; 0000 0273        overflow=1;
	SET
	BLD  R15,0
; 0000 0274     }
; 0000 0275     i_CMD_IN++;
_0x4E:
	CALL SUBOPT_0x9
; 0000 0276   }
; 0000 0277   if (overflow==1){
_0x4C:
	SBRS R15,0
	RJMP _0x51
; 0000 0278     if ((CMD_IN[i_CMD_IN-4]=='O')&&(CMD_IN[i_CMD_IN-3]=='B')&&(CMD_IN[i_CMD_IN-2]=='U')){
	CALL SUBOPT_0xA
	SBIW R30,4
	SUBI R30,LOW(-_CMD_IN)
	SBCI R31,HIGH(-_CMD_IN)
	LD   R26,Z
	CPI  R26,LOW(0x4F)
	BRNE _0x53
	CALL SUBOPT_0xA
	SBIW R30,3
	SUBI R30,LOW(-_CMD_IN)
	SBCI R31,HIGH(-_CMD_IN)
	LD   R26,Z
	CPI  R26,LOW(0x42)
	BRNE _0x53
	CALL SUBOPT_0xA
	SBIW R30,2
	SUBI R30,LOW(-_CMD_IN)
	SBCI R31,HIGH(-_CMD_IN)
	LD   R26,Z
	CPI  R26,LOW(0x55)
	BREQ _0x54
_0x53:
	RJMP _0x52
_0x54:
; 0000 0279        if (CMD_IN[0]!='#') putchar('#');
	LDS  R26,_CMD_IN
	CPI  R26,LOW(0x23)
	BREQ _0x55
	LDI  R30,LOW(35)
	ST   -Y,R30
	RCALL _putchar
; 0000 027A        for (j=0;j<i_CMD_IN;j++){
_0x55:
	__GETWRN 18,19,0
_0x57:
	CALL SUBOPT_0xB
	BRGE _0x58
; 0000 027B          putchar(CMD_IN[j]);
	CALL SUBOPT_0xC
	ST   -Y,R30
	RCALL _putchar
; 0000 027C        }
	__ADDWRN 18,19,1
	RJMP _0x57
_0x58:
; 0000 027D     }
; 0000 027E     else {
	RJMP _0x59
_0x52:
; 0000 027F        if (CMD_IN[0]!='#'){  //character '#' hilang
	LDS  R26,_CMD_IN
	CPI  R26,LOW(0x23)
	BREQ _0x5A
; 0000 0280          putchar('#');
	LDI  R30,LOW(35)
	ST   -Y,R30
	RCALL _putchar
; 0000 0281          putchar(CMD_IN[0]); //character 'V'
	LDS  R30,_CMD_IN
	ST   -Y,R30
	RCALL _putchar
; 0000 0282 
; 0000 0283          for (j=1;j<i_CMD_IN-1;j++){
	__GETWRN 18,19,1
_0x5C:
	CALL SUBOPT_0xA
	SBIW R30,1
	CP   R18,R30
	CPC  R19,R31
	BRGE _0x5D
; 0000 0284             Temp_Convert = CMD_IN[j]/16;
	CALL SUBOPT_0xC
	CALL SUBOPT_0xD
; 0000 0285             Temp_Convert2 = hexa_to_char(Temp_Convert);
; 0000 0286             putchar(Temp_Convert2);
; 0000 0287             Temp_Convert = CMD_IN[j]%16;
	CALL SUBOPT_0xE
; 0000 0288             Temp_Convert2 = hexa_to_char(Temp_Convert);
; 0000 0289             putchar(Temp_Convert2);
; 0000 028A             //delay_us(70);
; 0000 028B          }
	__ADDWRN 18,19,1
	RJMP _0x5C
_0x5D:
; 0000 028C 
; 0000 028D          putchar(CMD_IN[i_CMD_IN-1]);  //character '*'
	RJMP _0x136
; 0000 028E        }
; 0000 028F        else {
_0x5A:
; 0000 0290          for(j=0;j<2;j++) putchar(CMD_IN[j]);
	__GETWRN 18,19,0
_0x60:
	__CPWRN 18,19,2
	BRGE _0x61
	CALL SUBOPT_0xC
	ST   -Y,R30
	RCALL _putchar
	__ADDWRN 18,19,1
	RJMP _0x60
_0x61:
; 0000 0292 for (j=2;j<i_CMD_IN-1;j++){
	__GETWRN 18,19,2
_0x63:
	CALL SUBOPT_0xA
	SBIW R30,1
	CP   R18,R30
	CPC  R19,R31
	BRGE _0x64
; 0000 0293             Temp_Convert = CMD_IN[j]/16;
	CALL SUBOPT_0xC
	CALL SUBOPT_0xD
; 0000 0294             Temp_Convert2 = hexa_to_char(Temp_Convert);
; 0000 0295             putchar(Temp_Convert2);
; 0000 0296             Temp_Convert = CMD_IN[j]%16;
	CALL SUBOPT_0xE
; 0000 0297             Temp_Convert2 = hexa_to_char(Temp_Convert);
; 0000 0298             putchar(Temp_Convert2);
; 0000 0299             //delay_us(70);
; 0000 029A          }
	__ADDWRN 18,19,1
	RJMP _0x63
_0x64:
; 0000 029B 
; 0000 029C          putchar(CMD_IN[i_CMD_IN-1]);  //character '*'
_0x136:
	LDS  R30,_i_CMD_IN
	LDS  R31,_i_CMD_IN+1
	SBIW R30,1
	SUBI R30,LOW(-_CMD_IN)
	SBCI R31,HIGH(-_CMD_IN)
	LD   R30,Z
	ST   -Y,R30
	RCALL _putchar
; 0000 029D        }
; 0000 029E     }
_0x59:
; 0000 029F 
; 0000 02A0        i_CMD_IN=0;
	CALL SUBOPT_0x7
; 0000 02A1        overflow=0;
	CLT
	BLD  R15,0
; 0000 02A2        for (j=0;j<i_CMD_IN;j++) CMD_IN[j]=0;
	__GETWRN 18,19,0
_0x66:
	CALL SUBOPT_0xB
	BRGE _0x67
	CALL SUBOPT_0xF
	__ADDWRN 18,19,1
	RJMP _0x66
_0x67:
; 0000 02A3 state = 2;
	LDI  R21,LOW(2)
; 0000 02A4   }
; 0000 02A5 
; 0000 02A6 
; 0000 02A7   if (rx_buffer_overflow0==1){
_0x51:
	SBRS R2,4
	RJMP _0x68
; 0000 02A8     //#asm("cli")
; 0000 02A9 	//berhenti_interrupt_usart();
; 0000 02AA     key_prev=0;
	LDI  R30,LOW(0)
	STD  Y+2,R30
	STD  Y+2+1,R30
; 0000 02AB 	while((CMD_IN[key_prev]!='*') && (++key_prev<rx_counter0));
_0x69:
	LDD  R30,Y+2
	LDD  R31,Y+2+1
	SUBI R30,LOW(-_CMD_IN)
	SBCI R31,HIGH(-_CMD_IN)
	LD   R26,Z
	CPI  R26,LOW(0x2A)
	BREQ _0x6C
	LDD  R26,Y+2
	LDD  R27,Y+2+1
	ADIW R26,1
	STD  Y+2,R26
	STD  Y+2+1,R27
	MOV  R30,R7
	CALL SUBOPT_0x10
	BRLT _0x6D
_0x6C:
	RJMP _0x6B
_0x6D:
	RJMP _0x69
_0x6B:
; 0000 02AC 	i_CMD_IN=key_prev;
	LDD  R30,Y+2
	LDD  R31,Y+2+1
	STS  _i_CMD_IN,R30
	STS  _i_CMD_IN+1,R31
; 0000 02AD 	mulai_interrupt_usart();
	RCALL _mulai_interrupt_usart
; 0000 02AE 	if(((CMD_IN[0]=='#')&&(CMD_IN[1]=='S'))||(CMD_IN[0]=='S')){
	LDS  R26,_CMD_IN
	CPI  R26,LOW(0x23)
	BRNE _0x6F
	__GETB2MN _CMD_IN,1
	CPI  R26,LOW(0x53)
	BREQ _0x71
_0x6F:
	LDS  R26,_CMD_IN
	CPI  R26,LOW(0x53)
	BRNE _0x6E
_0x71:
; 0000 02AF 	    state = 0;
	LDI  R21,LOW(0)
; 0000 02B0 	}
; 0000 02B1     else if((CMD_IN[0]=='#')&&(CMD_IN[1]=='G')){
	RJMP _0x73
_0x6E:
	LDS  R26,_CMD_IN
	CPI  R26,LOW(0x23)
	BRNE _0x75
	__GETB2MN _CMD_IN,1
	CPI  R26,LOW(0x47)
	BREQ _0x76
_0x75:
	RJMP _0x74
_0x76:
; 0000 02B2 	    state = 1;
	LDI  R21,LOW(1)
; 0000 02B3 	}
; 0000 02B4     else {
	RJMP _0x77
_0x74:
; 0000 02B5 	    state = 2;
	LDI  R21,LOW(2)
; 0000 02B6 	}
_0x77:
_0x73:
; 0000 02B7     for(j=0;j<i_CMD_IN+1;j++){
	__GETWRN 18,19,0
_0x79:
	CALL SUBOPT_0xA
	ADIW R30,1
	CP   R18,R30
	CPC  R19,R31
	BRGE _0x7A
; 0000 02B8        dataSent[j] = CMD_IN[j];
	MOVW R30,R18
	MOVW R26,R28
	ADIW R26,8
	ADD  R30,R26
	ADC  R31,R27
	MOVW R0,R30
	CALL SUBOPT_0xC
	MOVW R26,R0
	ST   X,R30
; 0000 02B9     }
	__ADDWRN 18,19,1
	RJMP _0x79
_0x7A:
; 0000 02BA     counter_dataSent=i_CMD_IN;
	LDS  R16,_i_CMD_IN
; 0000 02BB         ///////
; 0000 02BC     //kosongkan buffer
; 0000 02BD     for(j=0;j<i_CMD_IN+1;j++){
	__GETWRN 18,19,0
_0x7C:
	CALL SUBOPT_0xA
	ADIW R30,1
	CP   R18,R30
	CPC  R19,R31
	BRGE _0x7D
; 0000 02BE        CMD_IN[j]=0;
	CALL SUBOPT_0xF
; 0000 02BF     }
	__ADDWRN 18,19,1
	RJMP _0x7C
_0x7D:
; 0000 02C0     i_CMD_IN=0;
	CALL SUBOPT_0x5
; 0000 02C1     rx_buffer_overflow0 = 0;
; 0000 02C2 	rx_counter0=0;
	CLR  R7
; 0000 02C3 
; 0000 02C4   }
; 0000 02C5 
; 0000 02C6   switch(state){
_0x68:
	MOV  R30,R21
; 0000 02C7        case 0:{
	CPI  R30,0
	BRNE _0x81
; 0000 02C8                 // mencari OBU yang terdeteksi
; 0000 02C9         	    // send "#S*"
; 0000 02CA                 for(j=0;j<(dataSent[j])+1;j++){
	__GETWRN 18,19,0
_0x83:
	CALL SUBOPT_0x11
	LD   R30,X
	CALL SUBOPT_0x12
	BRGE _0x84
; 0000 02CB                    dataSent[j]=0;
	CALL SUBOPT_0x11
	LDI  R30,LOW(0)
	ST   X,R30
; 0000 02CC                 }
	__ADDWRN 18,19,1
	RJMP _0x83
_0x84:
; 0000 02CD 
; 0000 02CE                 berhenti_interrupt_timer();
	RCALL _berhenti_interrupt_timer
; 0000 02CF                 dataOBU_complete = 0;
	CALL SUBOPT_0x6
; 0000 02D0                 wait_dataOBU_overflow = 0;
; 0000 02D1                 counter_send_command = 0;
; 0000 02D2 
; 0000 02D3         	    for(j=0;j<3;j++){
	__GETWRN 18,19,0
_0x86:
	__CPWRN 18,19,3
	BRGE _0x87
; 0000 02D4         	       ir_putchar(trigger[j]);
	MOVW R26,R28
	SUBI R26,LOW(-(142))
	SBCI R27,HIGH(-(142))
	ADD  R26,R18
	ADC  R27,R19
	CALL SUBOPT_0x13
; 0000 02D5         	       delay_us(70);
; 0000 02D6         	    }
	__ADDWRN 18,19,1
	RJMP _0x86
_0x87:
; 0000 02D7 		        state=0;
	LDI  R21,LOW(0)
; 0000 02D8 		        //#asm("sei")
; 0000 02D9                 //mulai_interrupt_usart();
; 0000 02DA                 delay_ms(150);
	LDI  R30,LOW(150)
	LDI  R31,HIGH(150)
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
; 0000 02DB         	    break;
	RJMP _0x80
; 0000 02DC               }
; 0000 02DD        case 1:{
_0x81:
	CPI  R30,LOW(0x1)
	BREQ PC+3
	JMP _0x88
; 0000 02DE                 // mengirim data
; 0000 02DF         	    // send "#G....*"
; 0000 02E0                 while ((!dataOBU_complete)&&(counter_send_command<=10)) {
_0x89:
	SBRC R2,0
	RJMP _0x8C
	LDI  R30,LOW(10)
	CP   R30,R9
	BRSH _0x8D
_0x8C:
	RJMP _0x8B
_0x8D:
; 0000 02E1         	          if(dataSent[0]=='#'){
	LDD  R26,Y+8
	CPI  R26,LOW(0x23)
	BRNE _0x8E
; 0000 02E2                         for(j=0;j<2;j++){       //#G
	__GETWRN 18,19,0
_0x90:
	__CPWRN 18,19,2
	BRGE _0x91
; 0000 02E3         	               ir_putchar(dataSent[j]);
	CALL SUBOPT_0x11
	CALL SUBOPT_0x13
; 0000 02E4         	               delay_us(70);
; 0000 02E5         	            }
	__ADDWRN 18,19,1
	RJMP _0x90
_0x91:
; 0000 02E6 
; 0000 02E7                         for(j=2;j<counter_dataSent;j+=2){   //string yang diconvert
	__GETWRN 18,19,2
_0x93:
	MOV  R30,R16
	MOVW R26,R18
	CALL SUBOPT_0x10
	BRGE _0x94
; 0000 02E8                            Temp_Convert = convert_to_hexa(dataSent[j],dataSent[j+1]);
	CALL SUBOPT_0x11
	CALL SUBOPT_0x14
; 0000 02E9         	               ir_putchar(Temp_Convert);
; 0000 02EA         	               delay_us(70);
; 0000 02EB         	            }
	__ADDWRN 18,19,2
	RJMP _0x93
_0x94:
; 0000 02EC                       }
; 0000 02ED                       else {              //char '#' hilang
	RJMP _0x95
_0x8E:
; 0000 02EE                         ir_putchar('#');
	LDI  R30,LOW(35)
	ST   -Y,R30
	RCALL _ir_putchar
; 0000 02EF                         delay_us(70);
	__DELAY_USW 280
; 0000 02F0                         for(j=0;j<1;j++){       //G
	__GETWRN 18,19,0
_0x97:
	__CPWRN 18,19,1
	BRGE _0x98
; 0000 02F1         	               ir_putchar(dataSent[j]);
	CALL SUBOPT_0x11
	CALL SUBOPT_0x13
; 0000 02F2         	               delay_us(70);
; 0000 02F3         	            }
	__ADDWRN 18,19,1
	RJMP _0x97
_0x98:
; 0000 02F4 
; 0000 02F5                         for(j=1;j<counter_dataSent;j+=2){   //string yang diconvert
	__GETWRN 18,19,1
_0x9A:
	MOV  R30,R16
	MOVW R26,R18
	CALL SUBOPT_0x10
	BRGE _0x9B
; 0000 02F6                            Temp_Convert = convert_to_hexa(dataSent[j],dataSent[j+1]);
	CALL SUBOPT_0x11
	CALL SUBOPT_0x14
; 0000 02F7         	               ir_putchar(Temp_Convert);
; 0000 02F8         	               delay_us(70);
; 0000 02F9         	            }
	__ADDWRN 18,19,2
	RJMP _0x9A
_0x9B:
; 0000 02FA                       }
_0x95:
; 0000 02FB 
; 0000 02FC                       for(j=counter_dataSent;j<(counter_dataSent+1);j++){  //*
	MOV  R18,R16
	CLR  R19
_0x9D:
	MOV  R30,R16
	CALL SUBOPT_0x12
	BRGE _0x9E
; 0000 02FD         	             ir_putchar(dataSent[j]);
	CALL SUBOPT_0x11
	CALL SUBOPT_0x13
; 0000 02FE         	             delay_us(70);
; 0000 02FF         	          }
	__ADDWRN 18,19,1
	RJMP _0x9D
_0x9E:
; 0000 0300                       mulai_interrupt_timer();
	RCALL _mulai_interrupt_timer
; 0000 0301 					  while((!overflow)&&(!wait_dataOBU_overflow)){
_0x9F:
	SBRC R15,0
	RJMP _0xA2
	SBRS R2,1
	RJMP _0xA3
_0xA2:
	RJMP _0xA1
_0xA3:
; 0000 0302                            while (ir_rx_counter)
_0xA4:
	TST  R13
	BREQ _0xA6
; 0000 0303 					       {
; 0000 0304 						     Rcvd=ir_getchar();
	RCALL _ir_getchar
	MOV  R17,R30
; 0000 0305 					         if (Rcvd=='#'){
	CPI  R17,35
	BRNE _0xA7
; 0000 0306 							    i_CMD_IN=0;
	CALL SUBOPT_0x7
; 0000 0307 						     }
; 0000 0308 						     CMD_IN[i_CMD_IN]= Rcvd;
_0xA7:
	CALL SUBOPT_0x8
	ST   Z,R17
; 0000 0309 						     if ((CMD_IN[i_CMD_IN]=='*')||(i_CMD_IN>=22)){       //#OK*
	CALL SUBOPT_0x8
	LD   R26,Z
	CPI  R26,LOW(0x2A)
	BREQ _0xA9
	LDS  R26,_i_CMD_IN
	LDS  R27,_i_CMD_IN+1
	SBIW R26,22
	BRLT _0xA8
_0xA9:
; 0000 030A 							    overflow=1;
	SET
	BLD  R15,0
; 0000 030B                                 dataOBU_complete=1;
	BLD  R2,0
; 0000 030C 						     }
; 0000 030D 					         i_CMD_IN++;
_0xA8:
	CALL SUBOPT_0x9
; 0000 030E                              if((overflow)||(wait_dataOBU_overflow)){
	SBRC R15,0
	RJMP _0xAC
	SBRS R2,1
	RJMP _0xAB
_0xAC:
; 0000 030F                                break;
	RJMP _0xA6
; 0000 0310                              }
; 0000 0311 					       }
_0xAB:
	RJMP _0xA4
_0xA6:
; 0000 0312                       }
	RJMP _0x9F
_0xA1:
; 0000 0313 
; 0000 0314                       if (wait_dataOBU_overflow) counter_send_command++;     //tidak ada balasan dari OBU
	SBRC R2,1
	INC  R9
; 0000 0315                       wait_dataOBU_overflow = 0;
	CLT
	BLD  R2,1
; 0000 0316                 }
	RJMP _0x89
_0x8B:
; 0000 0317 
; 0000 0318                 i_CMD_IN=0;
	CALL SUBOPT_0x7
; 0000 0319                 overflow=0;
	CLT
	BLD  R15,0
; 0000 031A                 for(j=0;j<i_CMD_IN;j++) {
	__GETWRN 18,19,0
_0xB0:
	CALL SUBOPT_0xB
	BRGE _0xB1
; 0000 031B                    CMD_IN[j]=0;
	CALL SUBOPT_0xF
; 0000 031C                 }
	__ADDWRN 18,19,1
	RJMP _0xB0
_0xB1:
; 0000 031D                 for(j=0;j<(dataSent[j])+1;j++){
	__GETWRN 18,19,0
_0xB3:
	CALL SUBOPT_0x11
	LD   R30,X
	CALL SUBOPT_0x12
	BRGE _0xB4
; 0000 031E                    dataSent[j]=0;
	CALL SUBOPT_0x11
	LDI  R30,LOW(0)
	ST   X,R30
; 0000 031F                 }
	__ADDWRN 18,19,1
	RJMP _0xB3
_0xB4:
; 0000 0320 
; 0000 0321                 berhenti_interrupt_timer();
	RCALL _berhenti_interrupt_timer
; 0000 0322                 dataOBU_complete = 0;
	CALL SUBOPT_0x6
; 0000 0323                 wait_dataOBU_overflow = 0;
; 0000 0324                 counter_send_command = 0;
; 0000 0325 
; 0000 0326 		        state=2;
	LDI  R21,LOW(2)
; 0000 0327 		        //#asm("sei")
; 0000 0328 
; 0000 0329                 break;
; 0000 032A               }
; 0000 032B        case 2:{
_0x88:
; 0000 032C 
; 0000 032D         	    break;
; 0000 032E               }
; 0000 032F   }
_0x80:
; 0000 0330 
; 0000 0331 };
	RJMP _0x49
; 0000 0332 }
_0xB6:
	RJMP _0xB6
;
;void pc_switch(){
; 0000 0334 void pc_switch(){
; 0000 0335     MUX_A=0;
; 0000 0336     MUX_B=0;
; 0000 0337     DEMUX_A=0;
; 0000 0338     DEMUX_B=0;
; 0000 0339     DEMUX_OE=0;
; 0000 033A     delay_us(10);
; 0000 033B }
;
;void ir_switch(){
; 0000 033D void ir_switch(){
; 0000 033E     //UBRRL=0x26;
; 0000 033F     MUX_A=1;
; 0000 0340     MUX_B=0;
; 0000 0341     DEMUX_A=1;
; 0000 0342     DEMUX_B=0;
; 0000 0343     DEMUX_OE=0;
; 0000 0344     delay_us(10);
; 0000 0345 }
;
;void sc_switch(){
; 0000 0347 void sc_switch(){
; 0000 0348     //UBRRL=0x4D;
; 0000 0349     MUX_A=0;
; 0000 034A     MUX_B=1;
; 0000 034B     DEMUX_A=0;
; 0000 034C     DEMUX_B=1;
; 0000 034D     DEMUX_OE=0;
; 0000 034E     delay_us(10);
; 0000 034F }
;
;unsigned char hexa_to_char(unsigned char a){
; 0000 0351 unsigned char hexa_to_char(unsigned char a){
_hexa_to_char:
; 0000 0352    unsigned char b;
; 0000 0353 
; 0000 0354    if (a==0)b='0'; else if (a==1)b='1'; else if (a==2)b='2'; else if (a==3)b='3'; else if (a==4)b='4'; else if (a==5)b='5';
	ST   -Y,R17
;	a -> Y+1
;	b -> R17
	LDD  R30,Y+1
	CPI  R30,0
	BRNE _0xD5
	RJMP _0x137
_0xD5:
	LDD  R26,Y+1
	CPI  R26,LOW(0x1)
	BRNE _0xD7
	LDI  R17,LOW(49)
	RJMP _0xD8
_0xD7:
	LDD  R26,Y+1
	CPI  R26,LOW(0x2)
	BRNE _0xD9
	LDI  R17,LOW(50)
	RJMP _0xDA
_0xD9:
	LDD  R26,Y+1
	CPI  R26,LOW(0x3)
	BRNE _0xDB
	LDI  R17,LOW(51)
	RJMP _0xDC
_0xDB:
	LDD  R26,Y+1
	CPI  R26,LOW(0x4)
	BRNE _0xDD
	LDI  R17,LOW(52)
	RJMP _0xDE
_0xDD:
	LDD  R26,Y+1
	CPI  R26,LOW(0x5)
	BRNE _0xDF
	LDI  R17,LOW(53)
; 0000 0355    else if (a==6)b='6'; else if (a==7)b='7'; else if (a==8)b='8'; else if (a==9)b='9'; else if (a==10)b='A'; else if (a==11)b='B';
	RJMP _0xE0
_0xDF:
	LDD  R26,Y+1
	CPI  R26,LOW(0x6)
	BRNE _0xE1
	LDI  R17,LOW(54)
	RJMP _0xE2
_0xE1:
	LDD  R26,Y+1
	CPI  R26,LOW(0x7)
	BRNE _0xE3
	LDI  R17,LOW(55)
	RJMP _0xE4
_0xE3:
	LDD  R26,Y+1
	CPI  R26,LOW(0x8)
	BRNE _0xE5
	LDI  R17,LOW(56)
	RJMP _0xE6
_0xE5:
	LDD  R26,Y+1
	CPI  R26,LOW(0x9)
	BRNE _0xE7
	LDI  R17,LOW(57)
	RJMP _0xE8
_0xE7:
	LDD  R26,Y+1
	CPI  R26,LOW(0xA)
	BRNE _0xE9
	LDI  R17,LOW(65)
	RJMP _0xEA
_0xE9:
	LDD  R26,Y+1
	CPI  R26,LOW(0xB)
	BRNE _0xEB
	LDI  R17,LOW(66)
; 0000 0356    else if (a==12)b='C'; else if (a==13)b='D'; else if (a==14)b='E'; else if (a==15)b='F'; else b='0';
	RJMP _0xEC
_0xEB:
	LDD  R26,Y+1
	CPI  R26,LOW(0xC)
	BRNE _0xED
	LDI  R17,LOW(67)
	RJMP _0xEE
_0xED:
	LDD  R26,Y+1
	CPI  R26,LOW(0xD)
	BRNE _0xEF
	LDI  R17,LOW(68)
	RJMP _0xF0
_0xEF:
	LDD  R26,Y+1
	CPI  R26,LOW(0xE)
	BRNE _0xF1
	LDI  R17,LOW(69)
	RJMP _0xF2
_0xF1:
	LDD  R26,Y+1
	CPI  R26,LOW(0xF)
	BRNE _0xF3
	LDI  R17,LOW(70)
	RJMP _0xF4
_0xF3:
_0x137:
	LDI  R17,LOW(48)
; 0000 0357 
; 0000 0358    return b;
_0xF4:
_0xF2:
_0xF0:
_0xEE:
_0xEC:
_0xEA:
_0xE8:
_0xE6:
_0xE4:
_0xE2:
_0xE0:
_0xDE:
_0xDC:
_0xDA:
_0xD8:
	MOV  R30,R17
	LDD  R17,Y+0
	ADIW R28,2
	RET
; 0000 0359 }
;
;unsigned char convert_to_hexa(unsigned char a,unsigned char b){
; 0000 035B unsigned char convert_to_hexa(unsigned char a,unsigned char b){
_convert_to_hexa:
; 0000 035C     unsigned char temp1, temp2;
; 0000 035D     if (a=='0')a=0; else if(a=='1')a=1; else if(a=='2')a=2; else if(a=='3')a=3; else if(a=='4')a=4; else if(a=='5')a=5;
	ST   -Y,R17
	ST   -Y,R16
;	a -> Y+3
;	b -> Y+2
;	temp1 -> R17
;	temp2 -> R16
	LDD  R26,Y+3
	CPI  R26,LOW(0x30)
	BRNE _0xF5
	RJMP _0x138
_0xF5:
	LDD  R26,Y+3
	CPI  R26,LOW(0x31)
	BRNE _0xF7
	LDI  R30,LOW(1)
	RJMP _0x139
_0xF7:
	LDD  R26,Y+3
	CPI  R26,LOW(0x32)
	BRNE _0xF9
	LDI  R30,LOW(2)
	RJMP _0x139
_0xF9:
	LDD  R26,Y+3
	CPI  R26,LOW(0x33)
	BRNE _0xFB
	LDI  R30,LOW(3)
	RJMP _0x139
_0xFB:
	LDD  R26,Y+3
	CPI  R26,LOW(0x34)
	BRNE _0xFD
	LDI  R30,LOW(4)
	RJMP _0x139
_0xFD:
	LDD  R26,Y+3
	CPI  R26,LOW(0x35)
	BRNE _0xFF
	LDI  R30,LOW(5)
	RJMP _0x139
; 0000 035E     else if (a=='6')a=6; else if(a=='7')a=7; else if(a=='8')a=8; else if(a=='9')a=9; else if(a=='A')a=10; else if(a=='B')a=11;
_0xFF:
	LDD  R26,Y+3
	CPI  R26,LOW(0x36)
	BRNE _0x101
	LDI  R30,LOW(6)
	RJMP _0x139
_0x101:
	LDD  R26,Y+3
	CPI  R26,LOW(0x37)
	BRNE _0x103
	LDI  R30,LOW(7)
	RJMP _0x139
_0x103:
	LDD  R26,Y+3
	CPI  R26,LOW(0x38)
	BRNE _0x105
	LDI  R30,LOW(8)
	RJMP _0x139
_0x105:
	LDD  R26,Y+3
	CPI  R26,LOW(0x39)
	BRNE _0x107
	LDI  R30,LOW(9)
	RJMP _0x139
_0x107:
	LDD  R26,Y+3
	CPI  R26,LOW(0x41)
	BRNE _0x109
	LDI  R30,LOW(10)
	RJMP _0x139
_0x109:
	LDD  R26,Y+3
	CPI  R26,LOW(0x42)
	BRNE _0x10B
	LDI  R30,LOW(11)
	RJMP _0x139
; 0000 035F     else if (a=='C')a=12; else if(a=='D')a=13; else if(a=='E')a=14; else if(a=='F')a=15; else a=0;
_0x10B:
	LDD  R26,Y+3
	CPI  R26,LOW(0x43)
	BRNE _0x10D
	LDI  R30,LOW(12)
	RJMP _0x139
_0x10D:
	LDD  R26,Y+3
	CPI  R26,LOW(0x44)
	BRNE _0x10F
	LDI  R30,LOW(13)
	RJMP _0x139
_0x10F:
	LDD  R26,Y+3
	CPI  R26,LOW(0x45)
	BRNE _0x111
	LDI  R30,LOW(14)
	RJMP _0x139
_0x111:
	LDD  R26,Y+3
	CPI  R26,LOW(0x46)
	BRNE _0x113
	LDI  R30,LOW(15)
	RJMP _0x139
_0x113:
_0x138:
	LDI  R30,LOW(0)
_0x139:
	STD  Y+3,R30
; 0000 0360 
; 0000 0361     if (b=='0')b=0; else if(b=='1')b=1; else if(b=='2')b=2; else if(b=='3')b=3; else if(b=='4')b=4; else if(b=='5')b=5;
	LDD  R26,Y+2
	CPI  R26,LOW(0x30)
	BRNE _0x115
	RJMP _0x13A
_0x115:
	LDD  R26,Y+2
	CPI  R26,LOW(0x31)
	BRNE _0x117
	LDI  R30,LOW(1)
	RJMP _0x13B
_0x117:
	LDD  R26,Y+2
	CPI  R26,LOW(0x32)
	BRNE _0x119
	LDI  R30,LOW(2)
	RJMP _0x13B
_0x119:
	LDD  R26,Y+2
	CPI  R26,LOW(0x33)
	BRNE _0x11B
	LDI  R30,LOW(3)
	RJMP _0x13B
_0x11B:
	LDD  R26,Y+2
	CPI  R26,LOW(0x34)
	BRNE _0x11D
	LDI  R30,LOW(4)
	RJMP _0x13B
_0x11D:
	LDD  R26,Y+2
	CPI  R26,LOW(0x35)
	BRNE _0x11F
	LDI  R30,LOW(5)
	RJMP _0x13B
; 0000 0362     else if (b=='6')b=6; else if(b=='7')b=7; else if(b=='8')b=8; else if(b=='9')b=9; else if(b=='A')b=10; else if(b=='B')b=11;
_0x11F:
	LDD  R26,Y+2
	CPI  R26,LOW(0x36)
	BRNE _0x121
	LDI  R30,LOW(6)
	RJMP _0x13B
_0x121:
	LDD  R26,Y+2
	CPI  R26,LOW(0x37)
	BRNE _0x123
	LDI  R30,LOW(7)
	RJMP _0x13B
_0x123:
	LDD  R26,Y+2
	CPI  R26,LOW(0x38)
	BRNE _0x125
	LDI  R30,LOW(8)
	RJMP _0x13B
_0x125:
	LDD  R26,Y+2
	CPI  R26,LOW(0x39)
	BRNE _0x127
	LDI  R30,LOW(9)
	RJMP _0x13B
_0x127:
	LDD  R26,Y+2
	CPI  R26,LOW(0x41)
	BRNE _0x129
	LDI  R30,LOW(10)
	RJMP _0x13B
_0x129:
	LDD  R26,Y+2
	CPI  R26,LOW(0x42)
	BRNE _0x12B
	LDI  R30,LOW(11)
	RJMP _0x13B
; 0000 0363     else if (b=='C')b=12; else if(b=='D')b=13; else if(b=='E')b=14; else if(b=='F')b=15; else b=0;
_0x12B:
	LDD  R26,Y+2
	CPI  R26,LOW(0x43)
	BRNE _0x12D
	LDI  R30,LOW(12)
	RJMP _0x13B
_0x12D:
	LDD  R26,Y+2
	CPI  R26,LOW(0x44)
	BRNE _0x12F
	LDI  R30,LOW(13)
	RJMP _0x13B
_0x12F:
	LDD  R26,Y+2
	CPI  R26,LOW(0x45)
	BRNE _0x131
	LDI  R30,LOW(14)
	RJMP _0x13B
_0x131:
	LDD  R26,Y+2
	CPI  R26,LOW(0x46)
	BRNE _0x133
	LDI  R30,LOW(15)
	RJMP _0x13B
_0x133:
_0x13A:
	LDI  R30,LOW(0)
_0x13B:
	STD  Y+2,R30
; 0000 0364 
; 0000 0365     return ((a*16)+b);
	LDD  R30,Y+3
	LDI  R26,LOW(16)
	MUL  R30,R26
	MOVW R30,R0
	LDD  R26,Y+2
	ADD  R30,R26
	LDD  R17,Y+1
	LDD  R16,Y+0
_0x2060001:
	ADIW R28,4
	RET
; 0000 0366 }
;
;
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif

	.CSEG
_putchar:
putchar0:
     sbis usr,udre
     rjmp putchar0
     ld   r30,y
     out  udr,r30
	ADIW R28,1
	RET

	.CSEG

	.CSEG

	.DSEG
_rx_buffer0:
	.BYTE 0x46
_IrInTime:
	.BYTE 0x4
_ir_rx_buffer:
	.BYTE 0x100
_i_CMD_IN:
	.BYTE 0x2
_CMD_IN:
	.BYTE 0x190

	.CSEG
;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x0:
	OUT  0x33,R30
	LDI  R30,LOW(0)
	OUT  0x32,R30
	OUT  0x3C,R30
	IN   R30,0x39
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x1:
	LDI  R30,LOW(6)
	OUT  0x20,R30
	LDI  R30,LOW(0)
	OUT  0x20,R30
	LDI  R30,LOW(51)
	OUT  0x9,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x2:
	LDI  R27,0
	CPI  R26,LOW(0x100)
	LDI  R30,HIGH(0x100)
	CPC  R27,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:21 WORDS
SUBOPT_0x3:
	MOV  R30,R17
	LDI  R31,0
	MOVW R26,R28
	ADIW R26,1
	ADD  R26,R30
	ADC  R27,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:30 WORDS
SUBOPT_0x4:
	ST   -Y,R0
	ST   -Y,R1
	ST   -Y,R15
	ST   -Y,R22
	ST   -Y,R23
	ST   -Y,R24
	ST   -Y,R25
	ST   -Y,R26
	ST   -Y,R27
	ST   -Y,R30
	ST   -Y,R31
	IN   R30,SREG
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x5:
	LDI  R30,LOW(0)
	STS  _i_CMD_IN,R30
	STS  _i_CMD_IN+1,R30
	CLT
	BLD  R2,4
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x6:
	CLT
	BLD  R2,0
	BLD  R2,1
	CLR  R9
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x7:
	LDI  R30,LOW(0)
	STS  _i_CMD_IN,R30
	STS  _i_CMD_IN+1,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:9 WORDS
SUBOPT_0x8:
	LDS  R30,_i_CMD_IN
	LDS  R31,_i_CMD_IN+1
	SUBI R30,LOW(-_CMD_IN)
	SBCI R31,HIGH(-_CMD_IN)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x9:
	LDI  R26,LOW(_i_CMD_IN)
	LDI  R27,HIGH(_i_CMD_IN)
	LD   R30,X+
	LD   R31,X+
	ADIW R30,1
	ST   -X,R31
	ST   -X,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 10 TIMES, CODE SIZE REDUCTION:15 WORDS
SUBOPT_0xA:
	LDS  R30,_i_CMD_IN
	LDS  R31,_i_CMD_IN+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0xB:
	RCALL SUBOPT_0xA
	CP   R18,R30
	CPC  R19,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:15 WORDS
SUBOPT_0xC:
	LDI  R26,LOW(_CMD_IN)
	LDI  R27,HIGH(_CMD_IN)
	ADD  R26,R18
	ADC  R27,R19
	LD   R30,X
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:9 WORDS
SUBOPT_0xD:
	SWAP R30
	ANDI R30,0xF
	STD  Y+1,R30
	ST   -Y,R30
	CALL _hexa_to_char
	ST   Y,R30
	ST   -Y,R30
	CALL _putchar
	RJMP SUBOPT_0xC

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0xE:
	ANDI R30,LOW(0xF)
	STD  Y+1,R30
	ST   -Y,R30
	CALL _hexa_to_char
	ST   Y,R30
	ST   -Y,R30
	JMP  _putchar

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0xF:
	LDI  R26,LOW(_CMD_IN)
	LDI  R27,HIGH(_CMD_IN)
	ADD  R26,R18
	ADC  R27,R19
	LDI  R30,LOW(0)
	ST   X,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:12 WORDS
SUBOPT_0x10:
	LDI  R31,0
	CP   R26,R30
	CPC  R27,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 9 TIMES, CODE SIZE REDUCTION:13 WORDS
SUBOPT_0x11:
	MOVW R26,R28
	ADIW R26,8
	ADD  R26,R18
	ADC  R27,R19
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x12:
	SUBI R30,-LOW(1)
	MOVW R26,R18
	RJMP SUBOPT_0x10

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:15 WORDS
SUBOPT_0x13:
	LD   R30,X
	ST   -Y,R30
	CALL _ir_putchar
	__DELAY_USW 280
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:16 WORDS
SUBOPT_0x14:
	LD   R30,X
	ST   -Y,R30
	MOVW R30,R18
	ADIW R30,1
	MOVW R26,R28
	ADIW R26,9
	ADD  R26,R30
	ADC  R27,R31
	LD   R30,X
	ST   -Y,R30
	CALL _convert_to_hexa
	STD  Y+1,R30
	ST   -Y,R30
	CALL _ir_putchar
	__DELAY_USW 280
	RET


	.CSEG
_delay_ms:
	ld   r30,y+
	ld   r31,y+
	adiw r30,0
	breq __delay_ms1
__delay_ms0:
	__DELAY_USW 0xFA0
	wdr
	sbiw r30,1
	brne __delay_ms0
__delay_ms1:
	ret

__DIVB21U:
	CLR  R0
	LDI  R25,8
__DIVB21U1:
	LSL  R26
	ROL  R0
	SUB  R0,R30
	BRCC __DIVB21U2
	ADD  R0,R30
	RJMP __DIVB21U3
__DIVB21U2:
	SBR  R26,1
__DIVB21U3:
	DEC  R25
	BRNE __DIVB21U1
	MOV  R30,R26
	MOV  R26,R0
	RET

__SAVELOCR4:
	ST   -Y,R19
__SAVELOCR3:
	ST   -Y,R18
__SAVELOCR2:
	ST   -Y,R17
	ST   -Y,R16
	RET

__LOADLOCR4:
	LDD  R19,Y+3
__LOADLOCR3:
	LDD  R18,Y+2
__LOADLOCR2:
	LDD  R17,Y+1
	LD   R16,Y
	RET

__INITLOCB:
__INITLOCW:
	ADD  R26,R28
	ADC  R27,R29
__INITLOC0:
	LPM  R0,Z+
	ST   X+,R0
	DEC  R24
	BRNE __INITLOC0
	RET

;END OF CODE MARKER
__END_OF_CODE:
