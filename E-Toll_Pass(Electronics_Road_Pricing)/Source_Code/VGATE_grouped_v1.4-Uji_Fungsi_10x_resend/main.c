/*****************************************************
This program was produced by the
CodeWizardAVR V1.25.5 Professional
Automatic Program Generator
� Copyright 1998-2010 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Project : GATE
Version : 
Date    : 25/04/2014
Author  : Ghozali S.H.
Company : 
Comments: 


Chip type               : ATmega32
Program type            : Application
AVR Core Clock frequency: 16.000000 MHz
Memory model            : Small
External RAM size       : 0
Data Stack size         : 512
*****************************************************/

#include <mega32a.h>
#include <delay.h>
//#include <ir_trx.c>
// Standard Input/Output functions
#include <stdio.h>

#ifndef RXB8
#define RXB8 1
#endif

#ifndef TXB8
#define TXB8 0
#endif

#ifndef UPE
#define UPE 2
#endif

#ifndef DOR
#define DOR 3
#endif

#ifndef FE
#define FE 4
#endif

#ifndef UDRE
#define UDRE 5
#endif

#ifndef RXC
#define RXC 7
#endif

#define FRAMING_ERROR (1<<FE)
#define PARITY_ERROR (1<<UPE)
#define DATA_OVERRUN (1<<DOR)
#define DATA_REGISTER_EMPTY (1<<UDRE)
#define RX_COMPLETE (1<<RXC)

#define DEMUX_A   PORTB.0   
#define DEMUX_B   PORTB.1
#define DEMUX_OE  PORTB.2
#define MUX_A     PORTB.3
#define MUX_B     PORTB.4

#define IR1_SD_O PORTA.0    //SHUTDOWN TFDU
#define IR1_RX_I PIND.2     //INT0
#define IR1_RX2_I PIND.3    //INT1
#define IR1_TX_O PORTA.2    //TX TFDU

#define IR1_TX_ON IR1_TX_O = 1
#define IR1_TX_OFF IR1_TX_O = 0

#define IR3_TX_O PORTA.1    //TX IR

#define IR3_TX_ON IR3_TX_O = 1
#define IR3_TX_OFF IR3_TX_O = 0

#define SD_ON 1
#define SD_OFF 0

#define HI 1
#define LO 0

#define IRTIME_DELAY1 1

// USART Receiver buffer
#define RX_BUFFER_SIZE0 70
char rx_buffer0[RX_BUFFER_SIZE0];

#if RX_BUFFER_SIZE0<=256
unsigned char rx_wr_index0,rx_rd_index0,rx_counter0;
#else
unsigned int rx_wr_index0,rx_rd_index0,rx_counter0;
#endif    

bit dataOBU_complete;
bit wait_dataOBU_overflow;
unsigned char counter_timer0;
unsigned char counter_send_command;


void mulai_interrupt_timer(){
TCCR0=0x03;  //250.000 kHz
TCNT0=0x00;
OCR0=0x00;
TIMSK|=0x01;
counter_timer0=0;
}

void berhenti_interrupt_timer(){
TCCR0=0x00;  
TCNT0=0x00;
OCR0=0x00;
TIMSK&=~(0x01);
}

void mulai_interrupt_usart(){
UCSRA=0x00;
UCSRB=0x98;
UCSRC=0x06;
UBRRH=0x00;
UBRRL=0x33;     //19200
}

void berhenti_interrupt_usart(){
UCSRA=0x00;
UCSRB=0x18;
UCSRC=0x06;
UBRRH=0x00;
UBRRL=0x33;     //19200
}


/************************* IR ********************************/
#define BIT_PER_CELL 2

#define PPM 4
#define PULSE_WIDTH 1
#define START_PULSE_WIDTH 3
#define CELL_INTERVAL 20
#define START_PULSE_NUM 2
#define IR_TIMER TCNT2

#define PULSE_INTERVAL (CELL_INTERVAL/PPM)

#define PULSE_WIDTH_DELAY delay_us(PULSE_WIDTH)
#define START_PULSE_WIDTH_DELAY delay_us(START_PULSE_WIDTH)
#define START_TIME 2
#define DATA_START_TIME 5
#define CELL_DELAY 20
#define PULSE_OFFSET 2

#define BYTE_WIDTH (PULSE_INTERVAL + CELL_INTERVAL*(PPM+1))

#define IR_OUT IR3_TX_ON
#define IR_OFF IR3_TX_OFF

#define NOT_PULSED GIFR==0
#define CLEAR_IR_FLAG GIFR=0xC0

//#define START_IR_TIMER TCCR2B=0x03;;IR_TIMER=0x00
#define START_IR_TIMER TCCR2=0x03;;IR_TIMER=0x00
#define STOP_IR_TIMER TCCR2=0x00
#define ENABLE_IR_TIMER_COMPARE_INT TIMSK|=0x80
#define SET_IR_TIMER_COMPARE OCR2=BYTE_WIDTH+CELL_DELAY
//#define SET_IR_TIMER_COMPARE OCR2B=BYTE_WIDTH+CELL_DELAY

#define TRUE 1
#define FALSE 0

#define MAX_CELL 4

#define NOT_SENSING_DATA 0
#define SENSING_DATA 1
#define IR_DATA_VALID

unsigned char Ir_Iteration, IrInTime[4];

// This flag is set when IR Receiver is sensing data
bit ir_status;

// IR Receiver buffer
#define IR_RX_BUFFER_SIZE 256
char ir_rx_buffer[IR_RX_BUFFER_SIZE];

#if IR_RX_BUFFER_SIZE<=256
unsigned char ir_rx_wr_index,ir_rx_rd_index,ir_rx_counter;
#else
unsigned int ir_rx_wr_index,ir_rx_rd_index,ir_rx_counter;
#endif

// This flag is set on IR Receiver buffer overflow
bit ir_rx_buffer_overflow;

// Handler for External Interrupt service routine

void ir_ext_int_handler(void)
{
// Place your code here
    CLEAR_IR_FLAG;
    
   if(ir_status == NOT_SENSING_DATA)
    {
	ENABLE_IR_TIMER_COMPARE_INT;
	SET_IR_TIMER_COMPARE;
	START_IR_TIMER;
	Ir_Iteration = 0;
	ir_status = SENSING_DATA;
    }
    else if(ir_status == SENSING_DATA)
    {
        if (Ir_Iteration<PPM ) IrInTime[Ir_Iteration%PPM] = IR_TIMER;
        Ir_Iteration++;
    }
}
// Handler for Timer output compare interrupt service routine
void ir_timer_comp_handler(void)
{
// Place your code here 
unsigned char u8Data,Iteration,temp;
//printf("%d %d %d %d \n", IrInTime[0],IrInTime[1],IrInTime[2],IrInTime[3]);

   STOP_IR_TIMER;
   ir_status = NOT_SENSING_DATA;
   u8Data = 0;
    //data conversion

   if (Ir_Iteration==PPM)
   {
    for (Iteration=0;Iteration<MAX_CELL;Iteration++) 
    {
       if (Iteration) u8Data <<=BIT_PER_CELL;
       
       temp = IrInTime[Iteration] - (Iteration*CELL_DELAY + DATA_START_TIME - PULSE_OFFSET);
       u8Data |= (temp/PULSE_INTERVAL);
    }
	
    ir_rx_buffer[ir_rx_wr_index]=u8Data;
    if (++ir_rx_wr_index == IR_RX_BUFFER_SIZE) ir_rx_wr_index=0;
    if (++ir_rx_counter == IR_RX_BUFFER_SIZE)
    {
      ir_rx_counter=0;
      ir_rx_buffer_overflow=1;
    };
   };
}


/*****************************************************
// Get byte from IR
*****************************************************/

unsigned char ir_getchar(void)
{
unsigned char u8Data;
while (ir_rx_counter==0);
u8Data=ir_rx_buffer[ir_rx_rd_index];
if (++ir_rx_rd_index == IR_RX_BUFFER_SIZE) ir_rx_rd_index=0;
#asm("cli")
--ir_rx_counter;
#asm("sei")
return u8Data;
}

/*****************************************************
// Send byte via IR
*****************************************************/
void ir_putchar(unsigned char u8Data)
{
    unsigned char Iteration, IrOutTime[MAX_CELL];
    
	#asm("cli")
	
    //data conversion
    for (Iteration=0;Iteration<MAX_CELL;Iteration++) 
    {
       switch (u8Data & 0xC0) 
       {
         case 0x00:// if DataCell = 0b 00bb bbbb --> ir_output(1000)
         IrOutTime[Iteration] = 0;
         break;

         case 0x40:// if DataCell = 0b 01bb bbbb --> ir_output(0100)
         IrOutTime[Iteration] = 5;
         break;

         case 0x80:// if DataCell = 0b 10bb bbbb --> ir_output(0010)
         IrOutTime[Iteration] = 10;
         break;

         case 0xC0:// if DataCell = 0b 11bb bbbb --> ir_output(0001)
         IrOutTime[Iteration] = 15;
         break;
       } //end switch
       IrOutTime[Iteration] += Iteration*CELL_DELAY + DATA_START_TIME;
       //printf("IrOutTime: %d %d %d %d", IrOutTime[0],IrOutTime[1],IrOutTime[2],IrOutTime[3]);
       u8Data <<=BIT_PER_CELL;
    }; // end for

    //start sending IR signal
            
    //start bit
    START_IR_TIMER;
    IR_OUT;
    START_PULSE_WIDTH_DELAY;
    IR_OFF;
	while(IR_TIMER < START_TIME);
    IR_OUT;
    START_PULSE_WIDTH_DELAY;
    IR_OFF;
	
    //data bit
    for (Iteration=0;Iteration<MAX_CELL;Iteration++) 
    {
        while(IR_TIMER<IrOutTime[Iteration]);
        IR_OUT;
        PULSE_WIDTH_DELAY;
        IR_OFF;
    }; // end for
	#asm("sei")
    while(IR_TIMER<=BYTE_WIDTH);
};

////////////////////////////////////////////////////////


void pc_switch();
void ir_switch();
void sc_switch();
unsigned char hexa_to_char(unsigned char a);
unsigned char convert_to_hexa(unsigned char a,unsigned char b);


// This flag is set on USART Receiver buffer overflow
bit rx_buffer_overflow0;
  
/*****************************************************
// Declare your global variables here
*****************************************************/
                 
int i_CMD_IN;
unsigned char CMD_IN[400];

//int count_timer;

bit int_flag;

// USART Receiver interrupt service routine
interrupt [USART_RXC] void usart_rx_isr(void)
{
char status,data;
status=UCSRA;
data=UDR;
if ((status & (FRAMING_ERROR | PARITY_ERROR | DATA_OVERRUN))==0)
   {
    //rx_buffer[rx_wr_index++]=data;
    CMD_IN[rx_counter0]=data;
    if(data=='#' || rx_counter0>0) 
    {        
        if(data=='#') {
           rx_counter0=0;
           i_CMD_IN=0;
           rx_buffer_overflow0=0;
        } 
        if (++rx_wr_index0 == RX_BUFFER_SIZE0) rx_wr_index0=0;
        if (++rx_counter0 >= 70 || data=='*')  
        {
           //rx_counter=0;
           //#asm("cli")  
           berhenti_interrupt_usart();
           rx_buffer_overflow0=1;
           int_flag=1; 
           /////////////////////////////////////
           //if ((CMD_IN[rx_counter0-4]=='#')&&(CMD_IN[rx_counter0-3]=='O')&&(CMD_IN[rx_counter0-2]=='K')&&(CMD_IN[rx_counter0-1]=='*')){   //data complete jika ada konfirmasi #ok*
           //    dataOBU_complete=1;
           //}                                  
           // atau ada balasan kartu kalau transaksi berhasil (meskipun sebelumnya command konfirmasi #OKE* tidak )
           //if((CMD_IN[0]=='#')&&(CMD_IN[1]=='V')&&(CMD_IN[rx_counter0-3]=='3')&&(CMD_IN[rx_counter0-2]=='3')&&(CMD_IN[rx_counter0-1]=='*')){
           //    dataOBU_complete=1;
           //}                                    
           /////////////////////////////////////
                
        }
    } 
   }
}

#ifndef _DEBUG_TERMINAL_IO_
// Get a character from the USART Receiver buffer
#define _ALTERNATE_GETCHAR_
#pragma used+
char getchar(void)
{
char data;
while (rx_counter0==0);
data=rx_buffer0[rx_rd_index0];
if (++rx_rd_index0 == RX_BUFFER_SIZE0) rx_rd_index0=0;
#asm("cli")
--rx_counter0;
#asm("sei")
return data;
}
#pragma used-
#endif

/*****************************************************
// External Interrupt 0 service routine
*****************************************************/
interrupt [EXT_INT0] void ext_int0_isr(void)
{
    ir_ext_int_handler();
} 

/*****************************************************
// External Interrupt 1 service routine
*****************************************************/
interrupt [EXT_INT1] void ext_int1_isr(void)
{
    ir_ext_int_handler();
}

// Timer 0 overflow interrupt service routine
interrupt [TIM0_OVF] void timer0_ovf_isr(void)    //interrupt setiap 1.02 ms
{
    // Place your code here
    counter_timer0++;
    if(counter_timer0 >= 40) wait_dataOBU_overflow = 1;
}


/*****************************************************
// Timer 2 output compare interrupt service routine
*****************************************************/
interrupt [TIM2_COMP] void timer2_comp_isr(void)
{
    ir_timer_comp_handler();
}

// Declare your global variables here

void main(void)
{

// Declare your local variables here   
bit overflow;
unsigned char Rcvd;
int j;
unsigned char gheader[3] = {'#','G','|'}; 
unsigned char start[5] = {'#','G','|','0','*'};    
unsigned char trigger[3] = {'#','S','*'};
unsigned char debt1[]={'0','3','5','>','7','7','8','<','>',';','>','8','4','?','?','8','9','3','0','4','9','8','2','9','6','=',
                            '4','6','3','0','=','?','1',';'};
unsigned char dataSent[100];
unsigned char counter_dataSent;      
unsigned char sCeksum[2];   
unsigned char state;  
unsigned char Temp;
unsigned char hTemp;
unsigned char ceksum;
int key_prev;
unsigned char Temp_Convert;
unsigned char Temp_Convert2;
    

// Input/Output Ports initialization
// Port A initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
PORTA=0x00;
DDRA=0x07;

// Port B initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
PORTB=0x00;
DDRB=0x1F;

// Port C initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
PORTC=0x00;
DDRC=0x00;

// Port D initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
PORTD=0x00;
DDRD=0x00;

// Timer/Counter 0 initialization
// Clock source: System Clock
// Clock value: Timer 0 Stopped
// Mode: Normal top=0xFF
// OC0 output: Disconnected
TCCR0=0x00;
TCNT0=0x00;
OCR0=0x00;

// Timer/Counter 1 initialization
// Clock source: System Clock
// Clock value: Timer1 Stopped
// Mode: Normal top=0xFFFF
// OC1A output: Discon.
// OC1B output: Discon.
// Noise Canceler: Off
// Input Capture on Falling Edge
// Timer1 Overflow Interrupt: Off
// Input Capture Interrupt: Off
// Compare A Match Interrupt: Off
// Compare B Match Interrupt: Off
TCCR1A=0x00;
TCCR1B=0x00;
TCNT1H=0x00;
TCNT1L=0x00;
ICR1H=0x00;
ICR1L=0x00;
OCR1AH=0x00;
OCR1AL=0x00;
OCR1BH=0x00;
OCR1BL=0x00;

// Timer/Counter 2 initialization
// Clock source: System Clock
// Clock value: Timer2 Stopped
// Mode: Normal top=0xFF
// OC2 output: Disconnected
ASSR=0x00;
TCCR2=0x00;
TCNT2=0x00;
OCR2=0x00;

// External Interrupt(s) initialization
// INT0: On
// INT0 Mode: Falling Edge
// INT1: On
// INT1 Mode: Falling Edge
// INT2: Off

GICR|=0xC0;
MCUCR=0x0A;
MCUCSR=0x00;
GIFR=0xC0;
//GICR|=0x80;
//MCUCR=0x08;
//MCUCSR=0x00;
//GIFR=0x80;

// Timer(s)/Counter(s) Interrupt(s) initialization
TIMSK=0x00;

// USART initialization
// Communication Parameters: 8 Data, 1 Stop, No Parity
// USART Receiver: On
// USART Transmitter: On
// USART Mode: Asynchronous
// USART Baud Rate: 76800
UCSRA=0x00;
UCSRB=0x98;
UCSRC=0x06;
UBRRH=0x00;
//UBRRL=0x67;   
UBRRL=0x33;  //19200
//UBRRL=0x19;  //38400

// Analog Comparator initialization
// Analog Comparator: Off
// Analog Comparator Input Capture by Timer/Counter 1: Off
ACSR=0x80;
SFIOR=0x00;

// ADC initialization
// ADC disabled
ADCSRA=0x00;

// SPI initialization
// SPI disabled
SPCR=0x00;

// TWI initialization
// TWI disabled
TWCR=0x00;

//IR1_TX_ON; //setting TFDU IR1
delay_us(1);
IR1_SD_O = SD_ON;
delay_us(10);
IR1_SD_O = SD_OFF;
delay_us(1);
IR1_TX_OFF;
delay_ms(2);


// Global enable interrupts
ir_rx_counter=0;
                      
//pc_switch();
//delay_ms(100);

// Global enable interrupts


state=2; 
i_CMD_IN=0; 
rx_buffer_overflow0 = 0;
dataOBU_complete = 0;
wait_dataOBU_overflow = 0;
counter_send_command = 0;
overflow=0;
#asm("sei")

while(1){

  if (ir_rx_counter)
  {                       
    state=2;
    Rcvd=ir_getchar();  
    if (Rcvd=='#'){
       i_CMD_IN=0;   
    }     
    CMD_IN[i_CMD_IN]= Rcvd;  
    if ((CMD_IN[i_CMD_IN]=='*')||(i_CMD_IN>=390)){
       overflow=1;     
    }                  
    i_CMD_IN++;
  }              
  if (overflow==1){
    if ((CMD_IN[i_CMD_IN-4]=='O')&&(CMD_IN[i_CMD_IN-3]=='B')&&(CMD_IN[i_CMD_IN-2]=='U')){
       if (CMD_IN[0]!='#') putchar('#');
       for (j=0;j<i_CMD_IN;j++){
         putchar(CMD_IN[j]); 
       }   
    } 
    else {
       if (CMD_IN[0]!='#'){  //character '#' hilang
         putchar('#'); 
         putchar(CMD_IN[0]); //character 'V' 
      
         for (j=1;j<i_CMD_IN-1;j++){
            Temp_Convert = CMD_IN[j]/16;
            Temp_Convert2 = hexa_to_char(Temp_Convert);
            putchar(Temp_Convert2);     
            Temp_Convert = CMD_IN[j]%16;
            Temp_Convert2 = hexa_to_char(Temp_Convert); 
            putchar(Temp_Convert2);
            //delay_us(70);
         }                           
      
         putchar(CMD_IN[i_CMD_IN-1]);  //character '*' 
       }
       else {      
         for(j=0;j<2;j++) putchar(CMD_IN[j]);
      
         for (j=2;j<i_CMD_IN-1;j++){
            Temp_Convert = CMD_IN[j]/16;
            Temp_Convert2 = hexa_to_char(Temp_Convert);
            putchar(Temp_Convert2);     
            Temp_Convert = CMD_IN[j]%16;
            Temp_Convert2 = hexa_to_char(Temp_Convert); 
            putchar(Temp_Convert2);
            //delay_us(70);
         }
      
         putchar(CMD_IN[i_CMD_IN-1]);  //character '*'
       }  
    }
                      
       i_CMD_IN=0;
       overflow=0;
       for (j=0;j<i_CMD_IN;j++) CMD_IN[j]=0; 
       state = 2;
  } 


  if (rx_buffer_overflow0==1){
    //#asm("cli")
	//berhenti_interrupt_usart();
    key_prev=0;
	while((CMD_IN[key_prev]!='*') && (++key_prev<rx_counter0));
	i_CMD_IN=key_prev;
	mulai_interrupt_usart();
	if(((CMD_IN[0]=='#')&&(CMD_IN[1]=='S'))||(CMD_IN[0]=='S')){
	    state = 0;
	}               	
    else if((CMD_IN[0]=='#')&&(CMD_IN[1]=='G')){
	    state = 1;
	} 
    else {
	    state = 2;
	}	    
    for(j=0;j<i_CMD_IN+1;j++){  
       dataSent[j] = CMD_IN[j];
    }     
    counter_dataSent=i_CMD_IN;
        ///////  
    //kosongkan buffer
    for(j=0;j<i_CMD_IN+1;j++){
       CMD_IN[j]=0;
    }   
    i_CMD_IN=0;   
    rx_buffer_overflow0 = 0;
	rx_counter0=0;
        
  }   
  
  switch(state){
       case 0:{     
                // mencari OBU yang terdeteksi
        	    // send "#S*"
                for(j=0;j<(dataSent[j])+1;j++){
                   dataSent[j]=0;
                }
                
                berhenti_interrupt_timer();
                dataOBU_complete = 0;
                wait_dataOBU_overflow = 0;
                counter_send_command = 0; 
                             
        	    for(j=0;j<3;j++){                                
        	       ir_putchar(trigger[j]); 
        	       delay_us(70);
        	    }            
		        state=0;  
		        //#asm("sei")        
                //mulai_interrupt_usart();
                delay_ms(150);
        	    break;
              }   
       case 1:{     
                // mengirim data
        	    // send "#G....*"  
                while ((!dataOBU_complete)&&(counter_send_command<=10)) {
        	          if(dataSent[0]=='#'){
                        for(j=0;j<2;j++){       //#G
        	               ir_putchar(dataSent[j]);
        	               delay_us(70);
        	            }      
                      
                        for(j=2;j<counter_dataSent;j+=2){   //string yang diconvert 
                           Temp_Convert = convert_to_hexa(dataSent[j],dataSent[j+1]);
        	               ir_putchar(Temp_Convert);
        	               delay_us(70);
        	            }                  
                      }
                      else {              //char '#' hilang
                        ir_putchar('#');
                        delay_us(70);
                        for(j=0;j<1;j++){       //G
        	               ir_putchar(dataSent[j]);
        	               delay_us(70);
        	            }      
                      
                        for(j=1;j<counter_dataSent;j+=2){   //string yang diconvert 
                           Temp_Convert = convert_to_hexa(dataSent[j],dataSent[j+1]);
        	               ir_putchar(Temp_Convert);
        	               delay_us(70);
        	            }
                      }
                      
                      for(j=counter_dataSent;j<(counter_dataSent+1);j++){  //*
        	             ir_putchar(dataSent[j]);
        	             delay_us(70);
        	          }                       
                      mulai_interrupt_timer();
					  while((!overflow)&&(!wait_dataOBU_overflow)){
                           while (ir_rx_counter)
					       {                       
						     Rcvd=ir_getchar();  
					         if (Rcvd=='#'){
							    i_CMD_IN=0;   
						     }     
						     CMD_IN[i_CMD_IN]= Rcvd;  
						     if ((CMD_IN[i_CMD_IN]=='*')||(i_CMD_IN>=22)){       //#OK*
							    overflow=1;    
                                dataOBU_complete=1;                                       
						     }                  
					         i_CMD_IN++;
                             if((overflow)||(wait_dataOBU_overflow)){
                               break;
                             }
					       }                                            
                      }
			 
                      if (wait_dataOBU_overflow) counter_send_command++;     //tidak ada balasan dari OBU  
                      wait_dataOBU_overflow = 0;                     
                }          
                
                i_CMD_IN=0;
                overflow=0;
                for(j=0;j<i_CMD_IN;j++) {
                   CMD_IN[j]=0;            
                }
                for(j=0;j<(dataSent[j])+1;j++){
                   dataSent[j]=0;
                }
                
                berhenti_interrupt_timer();
                dataOBU_complete = 0;
                wait_dataOBU_overflow = 0;
                counter_send_command = 0;          

		        state=2;
		        //#asm("sei")   
        
                break;
              }    
       case 2:{        
                 
        	    break;
              } 
  }  

};
}

void pc_switch(){
    MUX_A=0;
    MUX_B=0;
    DEMUX_A=0;
    DEMUX_B=0;
    DEMUX_OE=0;                         
    delay_us(10);
}

void ir_switch(){    
    //UBRRL=0x26;
    MUX_A=1;
    MUX_B=0;
    DEMUX_A=1;
    DEMUX_B=0;
    DEMUX_OE=0;    
    delay_us(10);
}

void sc_switch(){
    //UBRRL=0x4D;
    MUX_A=0;
    MUX_B=1;
    DEMUX_A=0;
    DEMUX_B=1;
    DEMUX_OE=0;                         
    delay_us(10);
}

unsigned char hexa_to_char(unsigned char a){
   unsigned char b; 

   if (a==0)b='0'; else if (a==1)b='1'; else if (a==2)b='2'; else if (a==3)b='3'; else if (a==4)b='4'; else if (a==5)b='5';
   else if (a==6)b='6'; else if (a==7)b='7'; else if (a==8)b='8'; else if (a==9)b='9'; else if (a==10)b='A'; else if (a==11)b='B';
   else if (a==12)b='C'; else if (a==13)b='D'; else if (a==14)b='E'; else if (a==15)b='F'; else b='0';

   return b;
}

unsigned char convert_to_hexa(unsigned char a,unsigned char b){
    unsigned char temp1, temp2;
    if (a=='0')a=0; else if(a=='1')a=1; else if(a=='2')a=2; else if(a=='3')a=3; else if(a=='4')a=4; else if(a=='5')a=5;
    else if (a=='6')a=6; else if(a=='7')a=7; else if(a=='8')a=8; else if(a=='9')a=9; else if(a=='A')a=10; else if(a=='B')a=11;
    else if (a=='C')a=12; else if(a=='D')a=13; else if(a=='E')a=14; else if(a=='F')a=15; else a=0;

    if (b=='0')b=0; else if(b=='1')b=1; else if(b=='2')b=2; else if(b=='3')b=3; else if(b=='4')b=4; else if(b=='5')b=5;
    else if (b=='6')b=6; else if(b=='7')b=7; else if(b=='8')b=8; else if(b=='9')b=9; else if(b=='A')b=10; else if(b=='B')b=11;
    else if (b=='C')b=12; else if(b=='D')b=13; else if(b=='E')b=14; else if(b=='F')b=15; else b=0;
    
    return ((a*16)+b);
}


