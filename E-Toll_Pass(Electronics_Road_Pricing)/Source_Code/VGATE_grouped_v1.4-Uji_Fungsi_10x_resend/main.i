
#pragma used+
sfrb TWBR=0;
sfrb TWSR=1;
sfrb TWAR=2;
sfrb TWDR=3;
sfrb ADCL=4;
sfrb ADCH=5;
sfrw ADCW=4;      
sfrb ADCSRA=6;
sfrb ADCSR=6;     
sfrb ADMUX=7;
sfrb ACSR=8;
sfrb UBRRL=9;
sfrb UCSRB=0xa;
sfrb UCSRA=0xb;
sfrb UDR=0xc;
sfrb SPCR=0xd;
sfrb SPSR=0xe;
sfrb SPDR=0xf;
sfrb PIND=0x10;
sfrb DDRD=0x11;
sfrb PORTD=0x12;
sfrb PINC=0x13;
sfrb DDRC=0x14;
sfrb PORTC=0x15;
sfrb PINB=0x16;
sfrb DDRB=0x17;
sfrb PORTB=0x18;
sfrb PINA=0x19;
sfrb DDRA=0x1a;
sfrb PORTA=0x1b;
sfrb EECR=0x1c;
sfrb EEDR=0x1d;
sfrb EEARL=0x1e;
sfrb EEARH=0x1f;
sfrw EEAR=0x1e;   
sfrb UBRRH=0x20;
sfrb UCSRC=0X20;
sfrb WDTCR=0x21;
sfrb ASSR=0x22;
sfrb OCR2=0x23;
sfrb TCNT2=0x24;
sfrb TCCR2=0x25;
sfrb ICR1L=0x26;
sfrb ICR1H=0x27;
sfrb OCR1BL=0x28;
sfrb OCR1BH=0x29;
sfrw OCR1B=0x28;  
sfrb OCR1AL=0x2a;
sfrb OCR1AH=0x2b;
sfrw OCR1A=0x2a;  
sfrb TCNT1L=0x2c;
sfrb TCNT1H=0x2d;
sfrw TCNT1=0x2c;  
sfrb TCCR1B=0x2e;
sfrb TCCR1A=0x2f;
sfrb SFIOR=0x30;
sfrb OSCCAL=0x31;
sfrb TCNT0=0x32;
sfrb TCCR0=0x33;
sfrb MCUCSR=0x34;
sfrb MCUCR=0x35;
sfrb TWCR=0x36;
sfrb SPMCR=0x37;
sfrb TIFR=0x38;
sfrb TIMSK=0x39;
sfrb GIFR=0x3a;
sfrb GICR=0x3b;
sfrb OCR0=0X3c;
sfrb SPL=0x3d;
sfrb SPH=0x3e;
sfrb SREG=0x3f;
#pragma used-

#asm
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
#endasm

#pragma used+

void delay_us(unsigned int n);
void delay_ms(unsigned int n);

#pragma used-

typedef char *va_list;

#pragma used+

char getchar(void);
void putchar(char c);
void puts(char *str);
void putsf(char flash *str);
int printf(char flash *fmtstr,...);
int sprintf(char *str, char flash *fmtstr,...);
int vprintf(char flash * fmtstr, va_list argptr);
int vsprintf(char *str, char flash * fmtstr, va_list argptr);

char *gets(char *str,unsigned int len);
int snprintf(char *str, unsigned int size, char flash *fmtstr,...);
int vsnprintf(char *str, unsigned int size, char flash * fmtstr, va_list argptr);

int scanf(char flash *fmtstr,...);
int sscanf(char *str, char flash *fmtstr,...);

#pragma used-

#pragma library stdio.lib

char rx_buffer0[70];

unsigned char rx_wr_index0,rx_rd_index0,rx_counter0;

bit dataOBU_complete;
bit wait_dataOBU_overflow;
unsigned char counter_timer0;
unsigned char counter_send_command;

void mulai_interrupt_timer(){
TCCR0=0x03;  
TCNT0=0x00;
OCR0=0x00;
TIMSK|=0x01;
counter_timer0=0;
}

void berhenti_interrupt_timer(){
TCCR0=0x00;  
TCNT0=0x00;
OCR0=0x00;
TIMSK&=~(0x01);
}

void mulai_interrupt_usart(){
UCSRA=0x00;
UCSRB=0x98;
UCSRC=0x06;
UBRRH=0x00;
UBRRL=0x33;     
}

void berhenti_interrupt_usart(){
UCSRA=0x00;
UCSRB=0x18;
UCSRC=0x06;
UBRRH=0x00;
UBRRL=0x33;     
}

unsigned char Ir_Iteration, IrInTime[4];

bit ir_status;

char ir_rx_buffer[256];

unsigned char ir_rx_wr_index,ir_rx_rd_index,ir_rx_counter;

bit ir_rx_buffer_overflow;

void ir_ext_int_handler(void)
{

GIFR=0xC0;

if(ir_status == 0)
{
TIMSK|=0x80;
OCR2=((20/4) + 20*(4+1))+20;
TCCR2=0x03;;TCNT2=0x00;
Ir_Iteration = 0;
ir_status = 1;
}
else if(ir_status == 1)
{
if (Ir_Iteration<4 ) IrInTime[Ir_Iteration%4] = TCNT2;
Ir_Iteration++;
}
}

void ir_timer_comp_handler(void)
{

unsigned char u8Data,Iteration,temp;

TCCR2=0x00;
ir_status = 0;
u8Data = 0;

if (Ir_Iteration==4)
{
for (Iteration=0;Iteration<4;Iteration++) 
{
if (Iteration) u8Data <<=2;

temp = IrInTime[Iteration] - (Iteration*20 + 5 - 2);
u8Data |= (temp/(20/4));
}

ir_rx_buffer[ir_rx_wr_index]=u8Data;
if (++ir_rx_wr_index == 256) ir_rx_wr_index=0;
if (++ir_rx_counter == 256)
{
ir_rx_counter=0;
ir_rx_buffer_overflow=1;
};
};
}

unsigned char ir_getchar(void)
{
unsigned char u8Data;
while (ir_rx_counter==0);
u8Data=ir_rx_buffer[ir_rx_rd_index];
if (++ir_rx_rd_index == 256) ir_rx_rd_index=0;
#asm("cli")
--ir_rx_counter;
#asm("sei")
return u8Data;
}

void ir_putchar(unsigned char u8Data)
{
unsigned char Iteration, IrOutTime[4];

#asm("cli")

for (Iteration=0;Iteration<4;Iteration++) 
{
switch (u8Data & 0xC0) 
{
case 0x00:
IrOutTime[Iteration] = 0;
break;

case 0x40:
IrOutTime[Iteration] = 5;
break;

case 0x80:
IrOutTime[Iteration] = 10;
break;

case 0xC0:
IrOutTime[Iteration] = 15;
break;
} 
IrOutTime[Iteration] += Iteration*20 + 5;

u8Data <<=2;
}; 

TCCR2=0x03;;TCNT2=0x00;
PORTA.1     = 1;
delay_us(3);
PORTA.1     = 0;
while(TCNT2 < 2);
PORTA.1     = 1;
delay_us(3);
PORTA.1     = 0;

for (Iteration=0;Iteration<4;Iteration++) 
{
while(TCNT2<IrOutTime[Iteration]);
PORTA.1     = 1;
delay_us(1);
PORTA.1     = 0;
}; 
#asm("sei")
while(TCNT2<=((20/4) + 20*(4+1)));
};

void pc_switch();
void ir_switch();
void sc_switch();
unsigned char hexa_to_char(unsigned char a);
unsigned char convert_to_hexa(unsigned char a,unsigned char b);

bit rx_buffer_overflow0;

int i_CMD_IN;
unsigned char CMD_IN[400];

bit int_flag;

interrupt [14] void usart_rx_isr(void)
{
char status,data;
status=UCSRA;
data=UDR;
if ((status & ((1<<4) | (1<<2) | (1<<3)))==0)
{

CMD_IN[rx_counter0]=data;
if(data=='#' || rx_counter0>0) 
{        
if(data=='#') {
rx_counter0=0;
i_CMD_IN=0;
rx_buffer_overflow0=0;
} 
if (++rx_wr_index0 == 70) rx_wr_index0=0;
if (++rx_counter0 >= 70 || data=='*')  
{

berhenti_interrupt_usart();
rx_buffer_overflow0=1;
int_flag=1; 

}
} 
}
}

#pragma used+
char getchar(void)
{
char data;
while (rx_counter0==0);
data=rx_buffer0[rx_rd_index0];
if (++rx_rd_index0 == 70) rx_rd_index0=0;
#asm("cli")
--rx_counter0;
#asm("sei")
return data;
}
#pragma used-

interrupt [2] void ext_int0_isr(void)
{
ir_ext_int_handler();
} 

interrupt [3] void ext_int1_isr(void)
{
ir_ext_int_handler();
}

interrupt [12] void timer0_ovf_isr(void)    
{

counter_timer0++;
if(counter_timer0 >= 40) wait_dataOBU_overflow = 1;
}

interrupt [5] void timer2_comp_isr(void)
{
ir_timer_comp_handler();
}

void main(void)
{

bit overflow;
unsigned char Rcvd;
int j;
unsigned char gheader[3] = {'#','G','|'}; 
unsigned char start[5] = {'#','G','|','0','*'};    
unsigned char trigger[3] = {'#','S','*'};
unsigned char debt1[]={'0','3','5','>','7','7','8','<','>',';','>','8','4','?','?','8','9','3','0','4','9','8','2','9','6','=',
'4','6','3','0','=','?','1',';'};
unsigned char dataSent[100];
unsigned char counter_dataSent;      
unsigned char sCeksum[2];   
unsigned char state;  
unsigned char Temp;
unsigned char hTemp;
unsigned char ceksum;
int key_prev;
unsigned char Temp_Convert;
unsigned char Temp_Convert2;

PORTA=0x00;
DDRA=0x07;

PORTB=0x00;
DDRB=0x1F;

PORTC=0x00;
DDRC=0x00;

PORTD=0x00;
DDRD=0x00;

TCCR0=0x00;
TCNT0=0x00;
OCR0=0x00;

TCCR1A=0x00;
TCCR1B=0x00;
TCNT1H=0x00;
TCNT1L=0x00;
ICR1H=0x00;
ICR1L=0x00;
OCR1AH=0x00;
OCR1AL=0x00;
OCR1BH=0x00;
OCR1BL=0x00;

ASSR=0x00;
TCCR2=0x00;
TCNT2=0x00;
OCR2=0x00;

GICR|=0xC0;
MCUCR=0x0A;
MCUCSR=0x00;
GIFR=0xC0;

TIMSK=0x00;

UCSRA=0x00;
UCSRB=0x98;
UCSRC=0x06;
UBRRH=0x00;

UBRRL=0x33;  

ACSR=0x80;
SFIOR=0x00;

ADCSRA=0x00;

SPCR=0x00;

TWCR=0x00;

delay_us(1);
PORTA.0     = 1;
delay_us(10);
PORTA.0     = 0;
delay_us(1);
PORTA.2     = 0;
delay_ms(2);

ir_rx_counter=0;

state=2; 
i_CMD_IN=0; 
rx_buffer_overflow0 = 0;
dataOBU_complete = 0;
wait_dataOBU_overflow = 0;
counter_send_command = 0;
overflow=0;
#asm("sei")

while(1){

if (ir_rx_counter)
{                       
state=2;
Rcvd=ir_getchar();  
if (Rcvd=='#'){
i_CMD_IN=0;   
}     
CMD_IN[i_CMD_IN]= Rcvd;  
if ((CMD_IN[i_CMD_IN]=='*')||(i_CMD_IN>=390)){
overflow=1;     
}                  
i_CMD_IN++;
}              
if (overflow==1){
if ((CMD_IN[i_CMD_IN-4]=='O')&&(CMD_IN[i_CMD_IN-3]=='B')&&(CMD_IN[i_CMD_IN-2]=='U')){
if (CMD_IN[0]!='#') putchar('#');
for (j=0;j<i_CMD_IN;j++){
putchar(CMD_IN[j]); 
}   
} 
else {
if (CMD_IN[0]!='#'){  
putchar('#'); 
putchar(CMD_IN[0]); 

for (j=1;j<i_CMD_IN-1;j++){
Temp_Convert = CMD_IN[j]/16;
Temp_Convert2 = hexa_to_char(Temp_Convert);
putchar(Temp_Convert2);     
Temp_Convert = CMD_IN[j]%16;
Temp_Convert2 = hexa_to_char(Temp_Convert); 
putchar(Temp_Convert2);

}                           

putchar(CMD_IN[i_CMD_IN-1]);  
}
else {      
for(j=0;j<2;j++) putchar(CMD_IN[j]);

for (j=2;j<i_CMD_IN-1;j++){
Temp_Convert = CMD_IN[j]/16;
Temp_Convert2 = hexa_to_char(Temp_Convert);
putchar(Temp_Convert2);     
Temp_Convert = CMD_IN[j]%16;
Temp_Convert2 = hexa_to_char(Temp_Convert); 
putchar(Temp_Convert2);

}

putchar(CMD_IN[i_CMD_IN-1]);  
}  
}

i_CMD_IN=0;
overflow=0;
for (j=0;j<i_CMD_IN;j++) CMD_IN[j]=0; 
state = 2;
} 

if (rx_buffer_overflow0==1){

key_prev=0;
while((CMD_IN[key_prev]!='*') && (++key_prev<rx_counter0));
i_CMD_IN=key_prev;
mulai_interrupt_usart();
if(((CMD_IN[0]=='#')&&(CMD_IN[1]=='S'))||(CMD_IN[0]=='S')){
state = 0;
}               	
else if((CMD_IN[0]=='#')&&(CMD_IN[1]=='G')){
state = 1;
} 
else {
state = 2;
}	    
for(j=0;j<i_CMD_IN+1;j++){  
dataSent[j] = CMD_IN[j];
}     
counter_dataSent=i_CMD_IN;

for(j=0;j<i_CMD_IN+1;j++){
CMD_IN[j]=0;
}   
i_CMD_IN=0;   
rx_buffer_overflow0 = 0;
rx_counter0=0;

}   

switch(state){
case 0:{     

for(j=0;j<(dataSent[j])+1;j++){
dataSent[j]=0;
}

berhenti_interrupt_timer();
dataOBU_complete = 0;
wait_dataOBU_overflow = 0;
counter_send_command = 0; 

for(j=0;j<3;j++){                                
ir_putchar(trigger[j]); 
delay_us(70);
}            
state=0;  

delay_ms(150);
break;
}   
case 1:{     

while ((!dataOBU_complete)&&(counter_send_command<=10)) {
if(dataSent[0]=='#'){
for(j=0;j<2;j++){       
ir_putchar(dataSent[j]);
delay_us(70);
}      

for(j=2;j<counter_dataSent;j+=2){   
Temp_Convert = convert_to_hexa(dataSent[j],dataSent[j+1]);
ir_putchar(Temp_Convert);
delay_us(70);
}                  
}
else {              
ir_putchar('#');
delay_us(70);
for(j=0;j<1;j++){       
ir_putchar(dataSent[j]);
delay_us(70);
}      

for(j=1;j<counter_dataSent;j+=2){   
Temp_Convert = convert_to_hexa(dataSent[j],dataSent[j+1]);
ir_putchar(Temp_Convert);
delay_us(70);
}
}

for(j=counter_dataSent;j<(counter_dataSent+1);j++){  
ir_putchar(dataSent[j]);
delay_us(70);
}                       
mulai_interrupt_timer();
while((!overflow)&&(!wait_dataOBU_overflow)){
while (ir_rx_counter)
{                       
Rcvd=ir_getchar();  
if (Rcvd=='#'){
i_CMD_IN=0;   
}     
CMD_IN[i_CMD_IN]= Rcvd;  
if ((CMD_IN[i_CMD_IN]=='*')||(i_CMD_IN>=22)){       
overflow=1;    
dataOBU_complete=1;                                       
}                  
i_CMD_IN++;
if((overflow)||(wait_dataOBU_overflow)){
break;
}
}                                            
}

if (wait_dataOBU_overflow) counter_send_command++;     
wait_dataOBU_overflow = 0;                     
}          

i_CMD_IN=0;
overflow=0;
for(j=0;j<i_CMD_IN;j++) {
CMD_IN[j]=0;            
}
for(j=0;j<(dataSent[j])+1;j++){
dataSent[j]=0;
}

berhenti_interrupt_timer();
dataOBU_complete = 0;
wait_dataOBU_overflow = 0;
counter_send_command = 0;          

state=2;

break;
}    
case 2:{        

break;
} 
}  

};
}

void pc_switch(){
PORTB.3=0;
PORTB.4=0;
PORTB.0   =0;
PORTB.1=0;
PORTB.2=0;                         
delay_us(10);
}

void ir_switch(){    

PORTB.3=1;
PORTB.4=0;
PORTB.0   =1;
PORTB.1=0;
PORTB.2=0;    
delay_us(10);
}

void sc_switch(){

PORTB.3=0;
PORTB.4=1;
PORTB.0   =0;
PORTB.1=1;
PORTB.2=0;                         
delay_us(10);
}

unsigned char hexa_to_char(unsigned char a){
unsigned char b; 

if (a==0)b='0'; else if (a==1)b='1'; else if (a==2)b='2'; else if (a==3)b='3'; else if (a==4)b='4'; else if (a==5)b='5';
else if (a==6)b='6'; else if (a==7)b='7'; else if (a==8)b='8'; else if (a==9)b='9'; else if (a==10)b='A'; else if (a==11)b='B';
else if (a==12)b='C'; else if (a==13)b='D'; else if (a==14)b='E'; else if (a==15)b='F'; else b='0';

return b;
}

unsigned char convert_to_hexa(unsigned char a,unsigned char b){
unsigned char temp1, temp2;
if (a=='0')a=0; else if(a=='1')a=1; else if(a=='2')a=2; else if(a=='3')a=3; else if(a=='4')a=4; else if(a=='5')a=5;
else if (a=='6')a=6; else if(a=='7')a=7; else if(a=='8')a=8; else if(a=='9')a=9; else if(a=='A')a=10; else if(a=='B')a=11;
else if (a=='C')a=12; else if(a=='D')a=13; else if(a=='E')a=14; else if(a=='F')a=15; else a=0;

if (b=='0')b=0; else if(b=='1')b=1; else if(b=='2')b=2; else if(b=='3')b=3; else if(b=='4')b=4; else if(b=='5')b=5;
else if (b=='6')b=6; else if(b=='7')b=7; else if(b=='8')b=8; else if(b=='9')b=9; else if(b=='A')b=10; else if(b=='B')b=11;
else if (b=='C')b=12; else if(b=='D')b=13; else if(b=='E')b=14; else if(b=='F')b=15; else b=0;

return ((a*16)+b);
}

