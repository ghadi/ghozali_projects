#include <mega32.h>
#include <delay.h>
#include <stdio.h>

/*****************************************************
Put definition here
*****************************************************/
#define BIT_PER_CELL 2

#define PPM 4
#define PULSE_WIDTH 1
#define START_PULSE_WIDTH 3
#define CELL_INTERVAL 20
#define START_PULSE_NUM 2
#define IR_TIMER TCNT2

#define PULSE_INTERVAL (CELL_INTERVAL/PPM)

#define PULSE_WIDTH_DELAY delay_us(PULSE_WIDTH)
#define START_PULSE_WIDTH_DELAY delay_us(START_PULSE_WIDTH)
#define START_TIME 2
#define DATA_START_TIME 5
#define CELL_DELAY 20
#define PULSE_OFFSET 2

#define BYTE_WIDTH (PULSE_INTERVAL + CELL_INTERVAL*(PPM+1))

#define IR_OUT IR3_TX_ON
#define IR_OFF IR3_TX_OFF

#define NOT_PULSED GIFR==0
#define CLEAR_IR_FLAG GIFR=0xC0

//#define START_IR_TIMER TCCR2B=0x03;;IR_TIMER=0x00
#define START_IR_TIMER TCCR2=0x03;;IR_TIMER=0x00
#define STOP_IR_TIMER TCCR2=0x00
#define ENABLE_IR_TIMER_COMPARE_INT TIMSK=0x80
#define SET_IR_TIMER_COMPARE OCR2=BYTE_WIDTH+CELL_DELAY
//#define SET_IR_TIMER_COMPARE OCR2B=BYTE_WIDTH+CELL_DELAY

#define TRUE 1
#define FALSE 0

#define MAX_CELL 4

#define NOT_SENSING_DATA 0
#define SENSING_DATA 1
#define IR_DATA_VALID

unsigned char Ir_Iteration, IrInTime[4];

// This flag is set when IR Receiver is sensing data
bit ir_status;

// IR Receiver buffer
#define IR_RX_BUFFER_SIZE 255
char ir_rx_buffer[IR_RX_BUFFER_SIZE];

#if IR_RX_BUFFER_SIZE<256
unsigned char ir_rx_wr_index,ir_rx_rd_index,ir_rx_counter;
#else
unsigned int ir_rx_wr_index,ir_rx_rd_index,ir_rx_counter;
#endif

// This flag is set on IR Receiver buffer overflow
bit ir_rx_buffer_overflow;

// Handler for External Interrupt service routine

void ir_ext_int_handler(void)
{
// Place your code here
    CLEAR_IR_FLAG;
    
   if(ir_status == NOT_SENSING_DATA)
    {
	ENABLE_IR_TIMER_COMPARE_INT;
	SET_IR_TIMER_COMPARE;
	START_IR_TIMER;
	Ir_Iteration = 0;
	ir_status = SENSING_DATA;
    }
    else if(ir_status == SENSING_DATA)
    {
        if (Ir_Iteration<PPM ) IrInTime[Ir_Iteration%PPM] = IR_TIMER;
        Ir_Iteration++;
    }
}
// Handler for Timer output compare interrupt service routine
void ir_timer_comp_handler(void)
{
// Place your code here 
unsigned char u8Data,Iteration,temp;
//printf("%d %d %d %d \n", IrInTime[0],IrInTime[1],IrInTime[2],IrInTime[3]);

   STOP_IR_TIMER;
   ir_status = NOT_SENSING_DATA;
   u8Data = 0;
    //data conversion

   if (Ir_Iteration==PPM)
   {
    for (Iteration=0;Iteration<MAX_CELL;Iteration++) 
    {
       if (Iteration) u8Data <<=BIT_PER_CELL;
       
       temp = IrInTime[Iteration] - (Iteration*CELL_DELAY + DATA_START_TIME - PULSE_OFFSET);
       u8Data |= (temp/PULSE_INTERVAL);
    }
	
    ir_rx_buffer[ir_rx_wr_index]=u8Data;
    if (++ir_rx_wr_index == IR_RX_BUFFER_SIZE) ir_rx_wr_index=0;
    if (++ir_rx_counter == IR_RX_BUFFER_SIZE)
    {
      ir_rx_counter=0;
      ir_rx_buffer_overflow=1;
    };
   };
}


/*****************************************************
// Get byte from IR
*****************************************************/

unsigned char ir_getchar(void)
{
unsigned char u8Data;
while (ir_rx_counter==0);
u8Data=ir_rx_buffer[ir_rx_rd_index];
if (++ir_rx_rd_index == IR_RX_BUFFER_SIZE) ir_rx_rd_index=0;
#asm("cli")
--ir_rx_counter;
#asm("sei")
return u8Data;
}

/*****************************************************
// Send byte via IR
*****************************************************/
void ir_putchar(unsigned char u8Data)
{
    unsigned char Iteration, IrOutTime[MAX_CELL];
    
	#asm("cli")
	
    //data conversion
    for (Iteration=0;Iteration<MAX_CELL;Iteration++) 
    {
       switch (u8Data & 0xC0) 
       {
         case 0x00:// if DataCell = 0b 00bb bbbb --> ir_output(1000)
         IrOutTime[Iteration] = 0;
         break;

         case 0x40:// if DataCell = 0b 01bb bbbb --> ir_output(0100)
         IrOutTime[Iteration] = 5;
         break;

         case 0x80:// if DataCell = 0b 10bb bbbb --> ir_output(0010)
         IrOutTime[Iteration] = 10;
         break;

         case 0xC0:// if DataCell = 0b 11bb bbbb --> ir_output(0001)
         IrOutTime[Iteration] = 15;
         break;
       } //end switch
       IrOutTime[Iteration] += Iteration*CELL_DELAY + DATA_START_TIME;
       //printf("IrOutTime: %d %d %d %d", IrOutTime[0],IrOutTime[1],IrOutTime[2],IrOutTime[3]);
       u8Data <<=BIT_PER_CELL;
    }; // end for

    //start sending IR signal
            
    //start bit
    START_IR_TIMER;
    IR_OUT;
    START_PULSE_WIDTH_DELAY;
    IR_OFF;
	while(IR_TIMER < START_TIME);
    IR_OUT;
    START_PULSE_WIDTH_DELAY;
    IR_OFF;
	
    //data bit
    for (Iteration=0;Iteration<MAX_CELL;Iteration++) 
    {
        while(IR_TIMER<IrOutTime[Iteration]);
        IR_OUT;
        PULSE_WIDTH_DELAY;
        IR_OFF;
    }; // end for
	#asm("sei")
    while(IR_TIMER<=BYTE_WIDTH);
};