/// -*- tab-width: 4; Mode: C++; c-basic-offset: 4; indent-tabs-mode: nil -*-

#ifdef USERHOOK_INIT
void userhook_init()
{
    // put your initialisation code here
    // this will be called once at start-up
}
#endif

////////////////////////////edited by Ghozali////////////////////////////////
//#ifdef USERHOOK_FASTLOOP

void userhook_FastLoop()
{
    // put your 100Hz code here
	//////////////add fixed wing program/////////////////
    // write out the servo PWM values
    // ------------------------------
	int16_t LastThrottle;
	
	//if (g.rc_5.radio_in <= 990) {FlightMode_VTOL = TAKEOFF_VTOL;}                   //STABILIZE
	//else if (g.rc_5.radio_in < 1200) {FlightMode_VTOL = TRANSITION_FLIGHT_VTOL;}    //ALT_HOLD
	//else if (g.rc_5.radio_in < 1800) {FlightMode_VTOL = CRUISE_VTOL;}               //SPORT
	//else if (g.rc_5.radio_in < 2040) {FlightMode_VTOL = TRANSITION_LANDING_VTOL;}   //DRIFT
	//else if (g.rc_5.radio_in >= 2040) {FlightMode_VTOL = LANDING_VTOL;}             //LAND

	if (g.rc_6.radio_in <= 1500){
		if (g.rc_7.radio_in <= 1200){
		   FlightMode_VTOL = TAKEOFF_VTOL;
		}
		else if ((g.rc_7.radio_in > 1200)&&(g.rc_7.radio_in <= 1800)){
		   FlightMode_VTOL = TRANSITION_FLIGHT_VTOL;
		}
		else {   ////g.rc_7.radio_in > 1800
		   FlightMode_VTOL = CRUISE_VTOL;
		}
	}
	else{  ////////////g.rc_6.radio_in > 1500
	    if (g.rc_7.radio_in <= 1200){
		   FlightMode_VTOL = TRANSITION_LANDING_VTOL;
		}
		else {   ////////g.rc_7.radio_in > 1200
		   FlightMode_VTOL = LANDING_VTOL;
		}
	}

	switch(FlightMode_VTOL){ 
	    case TAKEOFF_VTOL:   ////Namely STABILIZE
		     //Throttle and Control Surface of Fixed Wing OFF
			 control_modeFix=MANUAL;
		     //g.rc_5.radio_out = g.rc_3.radio_min;  // Throttle -> GhozaliCheck RPM is Zero
			 g.rc_5.radio_out = g.rc_5.radio_min;  // Throttle -> GhozaliCheck RPM is Zero
	         g.rc_6.radio_out = g.rc_2.radio_trim;  // Elevator
		     g.rc_7.radio_out = g.rc_4.radio_trim;  // Rudder
		     g.rc_8.radio_out = g.rc_1.radio_trim;  // Aileron/Flaperon Kanan
			 g.rc_9.radio_out = g.rc_1.radio_min+g.rc_1.radio_max-g.rc_1.radio_trim;  // Aileron/Flaperon Kiri
		     
			 //Multirotor on Manual Mode (STABILIZE)
		     control_mode = STABILIZE;
			 LastThrottle = g.rc_3.control_in;
		     break;

	    case TRANSITION_FLIGHT_VTOL:  ////Namely ALT_HOLD
	         //Fixed Wing on Manual Mode
		     //Control surface ON
		     control_modeFix=MANUAL;
			 g.rc_8.radio_out = g.rc_1.radio_in; //Roll Fixed Wing (Aileron/Flaperon Kanan)
			 g.rc_8.servo_out = g.rc_8.pwm_to_angle_dz(0);
			 if (g.rc_8.radio_in < 1500){ g.rc_9.radio_out = g.rc_1.radio_min + (g.rc_1.radio_max - g.rc_1.radio_in); }  //Aileron mode
			 else { g.rc_9.radio_out = g.rc_1.radio_in; }                                                                //Flap mode
			 g.rc_9.servo_out = g.rc_10.pwm_to_angle_dz(0); //Roll Fixed Wing (Aileron/Flaperon Kiri)
	         g.rc_6.radio_out = g.rc_2.radio_in; //Pitch Fixed Wing
			 g.rc_6.servo_out = g.rc_6.pwm_to_angle_dz(0);
	         //g.rc_5.radio_out = g.rc_3.radio_in; //Throttle Fixed Wing
			 g.rc_5.radio_out = g.rc_5.radio_in; //Throttle Fixed Wing
			 if ((g.rc_4.radio_in < (g.rc_4.radio_trim+7))||(g.rc_4.radio_in > (g.rc_4.radio_trim-7))) { 
				g.rc_7.radio_out = g.rc_4.radio_min + (g.rc_4.radio_max - g.rc_4.radio_in); 
			 }
	         else if (g.rc_4.radio_in > g.rc_4.radio_trim+7) { 
				g.rc_7.radio_out = g.rc_4.radio_min + (g.rc_4.radio_max - g.rc_4.radio_in) - 400; 
			 }
			 else if (g.rc_4.radio_in < g.rc_4.radio_trim-7) { 
				g.rc_7.radio_out = g.rc_4.radio_min + (g.rc_4.radio_max - g.rc_4.radio_in) + 400;  
			 } 
			 g.rc_7.servo_out = g.rc_7.pwm_to_angle_dz(0);  //Yaw Fixed Wing

	         ////set_servos();
			 //Initiate throttle (50% speed)
		     ////g.rc_5.radio_out = g.rc_5.radio_min+(int16_t)((g.rc_5.radio_max-g.rc_5.radio_min)*0.5f);
		     
			 //Multirotor on Stabilize (STABILIZE)
			 if (!motors.armed()){
			    init_arm_motors();
			 }
			 //control_mode = STABILIZE;
		     control_mode = ALT_HOLD;
			 //g.rc_3.radio_in = LastThrottle;
		     break;

	    case CRUISE_VTOL:  ////Namely SPORT
		     //Fixed Wing on Manual Mode (MANUAL)
		     control_modeFix=MANUAL;
			 g.rc_8.radio_out = g.rc_1.radio_in; //Roll Fixed Wing (Aileron/Flaperon Kanan)
			 g.rc_8.servo_out = g.rc_8.pwm_to_angle_dz(0);
			 if (g.rc_8.radio_in < 1500){ g.rc_9.radio_out = g.rc_1.radio_min + (g.rc_1.radio_max - g.rc_1.radio_in); }  //Aileron mode
			 else { g.rc_9.radio_out = g.rc_1.radio_in; }                                                                //Flap mode
			 g.rc_9.servo_out = g.rc_10.pwm_to_angle_dz(0); //Roll Fixed Wing (Aileron/Flaperon Kiri)
	         g.rc_6.radio_out = g.rc_2.radio_in; //Pitch Fixed Wing
			 g.rc_6.servo_out = g.rc_6.pwm_to_angle_dz(0);
	         //g.rc_5.radio_out = g.rc_3.radio_in; //Throttle Fixed Wing
			 g.rc_5.radio_out = g.rc_5.radio_in; //Throttle Fixed Wing
	         if ((g.rc_4.radio_in < (g.rc_4.radio_trim+7))||(g.rc_4.radio_in > (g.rc_4.radio_trim-7))) { 
				g.rc_7.radio_out = g.rc_4.radio_min + (g.rc_4.radio_max - g.rc_4.radio_in); 
			 }
	         else if (g.rc_4.radio_in > g.rc_4.radio_trim+7) { 
				g.rc_7.radio_out = g.rc_4.radio_min + (g.rc_4.radio_max - g.rc_4.radio_in) - 400; 
			 }
			 else if (g.rc_4.radio_in < g.rc_4.radio_trim-7) { 
				g.rc_7.radio_out = g.rc_4.radio_min + (g.rc_4.radio_max - g.rc_4.radio_in) + 400;  
			 } 
			 g.rc_7.servo_out = g.rc_7.pwm_to_angle_dz(0);  //Yaw Fixed Wing

			 ////set_servos();
			 //4 rotors of Multirotor OFF (STABILIZE)
			 init_disarm_motors();
			 g.rc_1.control_in = 0;
	         g.rc_2.control_in = 0;
	         g.rc_3.control_in = 0;
	         g.rc_4.control_in = 0;
			 ////control_mode = STABILIZE;
			 control_mode = SPORT;
			 break;
	
		case TRANSITION_LANDING_VTOL:   ////Namely DRIFT
			 //Fixed Wing on Manual Mode
			 //Control surface ON
			 control_modeFix=MANUAL;
			 g.rc_8.radio_out = g.rc_1.radio_in; //Roll Fixed Wing (Aileron/Flaperon Kanan)
			 g.rc_8.servo_out = g.rc_8.pwm_to_angle_dz(0);
			 if (g.rc_8.radio_in < 1500){ g.rc_9.radio_out = g.rc_1.radio_min + (g.rc_1.radio_max - g.rc_1.radio_in); }  //Aileron mode
			 else { g.rc_9.radio_out = g.rc_1.radio_in; }                                                                //Flap mode
			 g.rc_9.servo_out = g.rc_10.pwm_to_angle_dz(0); //Roll Fixed Wing (Aileron/Flaperon Kiri)
	         g.rc_6.radio_out = g.rc_2.radio_in; //Pitch Fixed Wing
			 g.rc_6.servo_out = g.rc_6.pwm_to_angle_dz(0);
	         //g.rc_5.radio_out = g.rc_3.radio_in; //Throttle Fixed Wing
			 g.rc_5.radio_out = g.rc_5.radio_in; //Throttle Fixed Wing
	         if ((g.rc_4.radio_in < (g.rc_4.radio_trim+7))||(g.rc_4.radio_in > (g.rc_4.radio_trim-7))) { 
				g.rc_7.radio_out = g.rc_4.radio_min + (g.rc_4.radio_max - g.rc_4.radio_in); 
			 }
	         else if (g.rc_4.radio_in > g.rc_4.radio_trim+7) { 
				g.rc_7.radio_out = g.rc_4.radio_min + (g.rc_4.radio_max - g.rc_4.radio_in) - 400; 
			 }
			 else if (g.rc_4.radio_in < g.rc_4.radio_trim-7) { 
				g.rc_7.radio_out = g.rc_4.radio_min + (g.rc_4.radio_max - g.rc_4.radio_in) + 400;  
			 } 
			 g.rc_7.servo_out = g.rc_7.pwm_to_angle_dz(0);  //Yaw Fixed Wing

			 ////set_servos();
			 //Multirotor on Stabilize (STABILIZE)
			 if (!motors.armed()){
			    init_arm_motors();
			 }
			 control_mode = DRIFT;
			 //g.rc_3.radio_in = LastThrottle;
			 break;
	
		case LANDING_VTOL:  ////Namely LAND
			 //Throttle and Control Surface of Fixed Wing OFF
			 control_modeFix=MANUAL;
			 //g.rc_5.radio_out = g.rc_3.radio_min;  // Throttle -> GhozaliCheck RPM is Zero
			 g.rc_5.radio_out = g.rc_5.radio_min;  // Throttle -> GhozaliCheck RPM is Zero
	         g.rc_6.radio_out = g.rc_2.radio_trim;  // Elevator
		     g.rc_7.radio_out = g.rc_4.radio_trim;  // Rudder
		     g.rc_8.radio_out = g.rc_1.radio_trim;  // Aileron/Flaperon Kanan
			 g.rc_9.radio_out = g.rc_1.radio_min+g.rc_1.radio_max-g.rc_1.radio_trim;  // Aileron/Flaperon Kiri (channel 9)

			 //Multirotor on Stabilize (STABILIZE)
			 if (!motors.armed()){
			    init_arm_motors();
			 }
			 control_mode = LAND;
			 break;
		default:
			 FlightMode_VTOL = TAKEOFF_VTOL; //default
			 break;
	}
	g.rc_5.output(); //Throttle Fixed Wing
	g.rc_6.output(); //Elevator Fixed Wing
	g.rc_7.output(); //Rudder Fixed Wing
	g.rc_8.output(); //Aileron/Flaperon Kanan Fixed Wing
	g.rc_9.output(); //Aileron/Flaperon Kiri Fixed Wing
	////////////////////////////////
}

//#endif
////////////////////////////////////////////////////////////////////


#ifdef USERHOOK_50HZLOOP
void userhook_50Hz()
{
    // put your 50Hz code here
}
#endif

#ifdef USERHOOK_MEDIUMLOOP
void userhook_MediumLoop()
{
    // put your 10Hz code here
}
#endif

#ifdef USERHOOK_SLOWLOOP
void userhook_SlowLoop()
{
    // put your 3.3Hz code here
}
#endif

#ifdef USERHOOK_SUPERSLOWLOOP
void userhook_SuperSlowLoop()
{
    // put your 1Hz code here
}
#endif